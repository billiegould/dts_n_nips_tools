#!/bin/bash

#REF="/scratch/base/misc/genomics/genomes/hg19/hg19.bwa_mem.fa" 

REF="/rnd/bgould/SDTS/balancing/Probe_selection/all_sdts_probes.fa"

INFILE=$(ls ${SGE_TASK_ID}_*_3.fastq.gz)

NAME=$(basename $INFILE _3.fastq.gz)

echo $NAME

/home/mark_theilmann/src/bwa-0.7.17/bwa mem -t 4 -T 0 $REF $INFILE | samtools view -bhu - | samtools sort -o ${NAME}_sorted.bam - && samtools index ${NAME}_sorted.bam 
