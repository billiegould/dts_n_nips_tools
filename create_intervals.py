# this script will take a list of snps for imputation and generate a list of imputation regions continaing those snps. One file per chromosome.

#useage create_intervals.py snp_list.csv <chr#>
import sys

positions = []
intervals = open((sys.argv[2] + "_Intervals.txt"), "w")

with open(sys.argv[1], "r") as sites:
    for line in sites:
	#	print line
	#	print line.split("_")[1]
		if int(line.split(" ")[0]) == int(sys.argv[2]):
			pos = int(line.split(" ")[1])
			positions.append(pos)
		else:
			continue

left = positions[0]
for i in range(1, len(positions)):
	if positions[i] - positions[i-1] <= 5000:
		continue
	elif positions[i] - positions[i-1] > 5000:
		intervals.write(str(left) + "\t" + str(positions[i-1]) + "\n")  #write th prev window
		print str(positions[i-1] - left)
		left = positions[i]

# get the last SNP/window
if positions[-1] - positions[-2] <= 5000:
	intervals.write(str(left) + "\t" + str(positions[-1]))
elif positions[-1] - positions[-2] > 5000:
	intervals.write(str(positions[-1]) + "\t" + str(positions[-1]))

intervals.close()

print("Complete. Writen to "  + str(sys.argv[2] + "_Intervals.txt"))
