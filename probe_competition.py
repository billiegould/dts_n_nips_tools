"""
This script will compare read counts between probes that are within a certain distance from one another.
Difference in counts is plotted against inter-probe distance. Calculations are made for a single sample.
Input is the output from samtools idxstats for the reads file that has been aligned to the probes.fa reference. This includes on and off target reads.
"""

import pysam
import sys
from collections import defaultdict
import matplotlib
matplotlib.use('agg')
from matplotlib import pyplot as plt


print("Probe bed file must be SORTED")

stats_file = sys.argv[1]
sample = stats_file[:16]

bed_file = sys.argv[2]

probe_pos_dict = defaultdict(list)
with open(bed_file, "r") as fp:
    print("Reading probe bed file . . .")
    for line in fp:
        chrom = line.split()[0]
        start = int(line.split()[1])
        name = line.split()[3]
        strand = line.split()[5]
        probe_pos_dict[(chrom, strand)].append((name, start))

counts_dict = {}
print("Reading probe counts . . .")
with open(stats_file, "r") as fp:
    for line in fp:
        probe, length, counts, _ = line.split()
        counts_dict[probe] = int(counts)

plot_dist = []
plot_diff = []
outfile = open(sample + ".competition", "w")
for chrom_strand in probe_pos_dict.keys():
    print("Processing: " + str(chrom_strand))
    probe_list = probe_pos_dict[chrom_strand]
    
    for i, probe_tuple in enumerate(probe_list):
        print(probe_tuple)
        probe, start = probe_tuple
        focal_probe_count = counts_dict[probe]
        for comp_probe, comp_probe_start in probe_list[ (i+1) : ]:
            if comp_probe_start - start <= 1000:
                plot_dist.append(comp_probe_start - start)
                plot_diff.append(focal_probe_count - counts_dict[comp_probe])
                #print(probe + "\t" + comp_probe + "\t" + str(comp_probe_start - start) + "\t" + str(focal_probe_count - counts_dict[comp_probe]))
                #outfile.write(probe + "\t" + comp_probe + "\t" + str(comp_probe_start - start) + "\t" + str(focal_probe_count - counts_dict[comp_probe]) + "\n")
            else:
                break

# plot the data
from numpy import array, log, sign
from scipy import stats

def semilog(x):
    xlog = sign(x) * log(abs(x))
    return xlog

#slope, intercept, r_value, p_value, std_err = stats.linregress(plot_dist,plot_diff)
#line = slope*array(plot_dist) + intercept

plt.figure(1)
y = [semilog(x) for x in array(plot_diff)]
#plt.plot(array(plot_dist), y, 'o', array(plot_dist), line)
plt.plot(array(plot_dist), y, 'o', color='black')
plt.ylabel("semilog(upstream probe - downstream probe)")
plt.xlabel("distance (bp) between probes")

plt.savefig(sample + "_Competition_Plot.png")

print("Complete")

        

    
