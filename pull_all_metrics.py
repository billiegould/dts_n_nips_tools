# This script will take a list of flowcells and import the all_metrics.table file from each one, from STOR. Files will be combined into one output CSV.
# Code snippet modified from script by K.Pierce

import os
import argparse
#from string import Template #now part of base
import pandas as pd
import stor


#def join_well_coordinates(stor_data):
#    stor_data = stor_data.assign(well_coor=[BARCODES[i] for i in stor_data.barcode])
#    return stor_data


def find_run_folder(flowcell_id):
    #print stor.Path('swift://AUTH_final_analysis_prod/' + str(flowcell_id)).listdir()
    path_list = [i for i in stor.Path('swift://AUTH_final_analysis_prod/').listdir() if flowcell_id in i]
    print path_list
    try:
        len(path_list) == 1
    except AssertionError:
        print 'More than one path returned for that flowcell ID'
    path = path_list[0] + '/analysis/metrics/all_metrics.table'
    
    return path


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    #parser.add_argument('-i', '--input_path', help='Path to data in object storage')
    parser.add_argument('-f', '--flowcell_ids', help='list of Flowcell IDS')
    #parser.add_argument('-r', '--run_folder', help='Run folder name')
    parser.add_argument('-o', '--output_path', help='Local path to save directory for output file', required=True)
    #parser.add_argument('-n', '--output_name', help='Name for output file', required=True)
    args = parser.parse_args()

    if not os.path.exists(args.output_path):
        os.makedirs(args.output_path)

    #if args.input_path:
    #    input_path = args.input_path

    dfs = []
    if args.flowcell_ids:
        with open(args.flowcell_ids, "r") as fc_list:
            for line in fc_list:
                if "Metrics" not in line: 
                    input_path = find_run_folder(line.strip())

                    data = pd.read_table(stor.Path(input_path).open('r'))

                    dfs.append(data)
    #elif args.run_folder:
    #    path_template = Template('swift://AUTH_final_analysis_prod/$s/analysis/metrics/all_metrics.table')
    #    input_path = path_template.substitute(s=opts.run_folder)

    else:
        print 'Must supply -f to gather data'

    print "Writing final CSV . . ."
    final_data = pd.concat(dfs)
    final_path = os.path.join(args.output_path, "Big_sample_metrics_table.csv")
    final_data.to_csv(final_path, index=False)
    
    print "Complete. Writen to " + str(final_path)
