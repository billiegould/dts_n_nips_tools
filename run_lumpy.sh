#! /bin/bash

INFILE="laneM_5520BUFFY_sort.bam"

SAMPLE="$(basename $INFILE _sort.bam)" 

DS="1"  #downsample to coverage > for direction to the right folders only

WKDIR="1kb_DEL/${DS}x"

NUM="${SGE_TASK_ID}"

if [ SAMPLE=="laneM_5517CULTURE1" ]
then
    mean=213.77;
    sdev=120.65;
    NAME="5517CULTURE1"
fi

if [ SAMPLE=="laneM_5518CULTURE1" ]
then
    mean=254.87;
    sdev=148.24;
    NAME="5518CULTURE1"
fi

if [ SAMPLE=="laneM_5520PELLET" ]
then
    mean=177.26;
    sdev=123.78;
    NAME="5520PELLET"
fi

if [ SAMPLE=="laneM_5520BUFFY" ]
then
    mean=214.58;
    sdev=132.82;
    NAME="5520BUFFY"    
fi


if [ ! -f ${WKDIR}/${SAMPLE}_${SGE_TASK_ID}_merged_sort.bam ]; then
    samtools view -Sb ${WKDIR}/${SAMPLE}_${SGE_TASK_ID}_merged.sam | samtools sort -@ 11 -o ${WKDIR}/${SAMPLE}_${SGE_TASK_ID}_merged_sort.bam - 
    samtools index ${WKDIR}/${SAMPLE}_${SGE_TASK_ID}_merged_sort.bam
fi

if [ ! -f ${WKDIR}/${SAMPLE}_${SGE_TASK_ID}_discords_sorted.bam ]; then
    samtools view -b -F 1294 ${WKDIR}/${SAMPLE}_${SGE_TASK_ID}_merged_sort.bam > ${WKDIR}/${SAMPLE}_${SGE_TASK_ID}_discords.bam
    samtools sort -o ${WKDIR}/${SAMPLE}_${SGE_TASK_ID}_discords_sorted.bam ${WKDIR}/${SAMPLE}_${SGE_TASK_ID}_discords.bam
fi

if [ ! -f ${WKDIR}/${SAMPLE}_${SGE_TASK_ID}_splits_sorted.bam ]; then
    samtools view -h ${WKDIR}/${SAMPLE}_${SGE_TASK_ID}_merged_sort.bam | extractSplitReads_BwaMem -i stdin | samtools view -Sb - > ${WKDIR}/${SAMPLE}_${SGE_TASK_ID}_splits.bam
    samtools sort -o ${WKDIR}/${SAMPLE}_${SGE_TASK_ID}_splits_sorted.bam ${WKDIR}/${SAMPLE}_${SGE_TASK_ID}_splits.bam
fi


lumpy -mw 4 -tt 0 -pe id:${NAME},bam_file:${WKDIR}/${SAMPLE}_${SGE_TASK_ID}_discords_sorted.bam,histo_file:${SAMPLE}.histo,mean:${mean},stdev:${sdev},read_length:151,min_non_overlap:151,discordant_z:5,back_distance:10,weight:1 -sr id:${NAME},bam_file:${WKDIR}/${SAMPLE}_${SGE_TASK_ID}_splits_sorted.bam,back_distance:10,weight:1 > ${WKDIR}/${SAMPLE}_${SGE_TASK_ID}_lumpy.vcf


grep -v "BND" ${WKDIR}/${SAMPLE}_${SGE_TASK_ID}_lumpy.vcf > ${WKDIR}/${SAMPLE}_${SGE_TASK_ID}_lumpy_noBND.vcf

bedtools intersect -a 1kb_beds/1kb_SVs_${SGE_TASK_ID}_sort.bed -b ${WKDIR}/${SAMPLE}_${SGE_TASK_ID}_lumpy_noBND.vcf -u -f 0.25 > ${WKDIR}/${SAMPLE}_${SGE_TASK_ID}_detected.txt

N=$(wc -l ${WKDIR}/${SAMPLE}_${SGE_TASK_ID}_detected.txt | cut -f1 -d " ")
echo "Detected SVs: " $N

D=$(wc -l 1kb_beds/1kb_SVs_${SGE_TASK_ID}_sort.bed | cut -f1 -d " ")
echo "total SVs: " $D

sen=$(echo "scale=4 ; ${N} / ${D}" |bc)
echo "Sen: " $sen

echo ${NAME}_1kb_DEL_${DS}x_${SGE_TASK_ID} $N $D $sen >> ${WKDIR}/1kb_DEL_summary.txt

rm ${WKDIR}/${SAMPLE}_${SGE_TASK_ID}_lumpy_noBND.vcf
rm ${WKDIR}/${SAMPLE}_${SGE_TASK_ID}_detected.txt
