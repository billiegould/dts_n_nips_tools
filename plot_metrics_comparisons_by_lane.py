
# this script will plot the data from a flowcell against metrics files agregated from produciton runs by pull_all_metrics.py
# separate plots will be output for lane 1 and lane 2 of the test flowcell.

import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt

from numpy import median
import seaborn as sns
import pandas as pd
import fire
import os
from random import randint


def make_lane_plots(analysis_folder):
    twelve_week_data = pd.read_csv('/rnd/bgould/SDTS/All_metrics_compare/Big_sample_metrics_table.csv', index_col=68)
    twelve_week_noNTC = twelve_week_data[twelve_week_data.dbsnp_rate != "None"]

    flowcell_data = pd.read_csv(analysis_folder + "/metrics/all_metrics.table", sep="\t")
    flowcell_noNTC = flowcell_data[flowcell_data.dbsnp_rate != "None"]
    for word in analysis_folder.split("/"):
        if word.startswith("H"):
            flowcell_name = word[0:9]
            print("Processing flowcell: " + flowcell_name)
            break

    lane1_data = flowcell_noNTC[ flowcell_noNTC['identifier'].str.contains("s_1_")]
    lane2_data = flowcell_noNTC[ flowcell_noNTC['identifier'].str.contains("s_2_")]

    for i, lane_data in enumerate([lane1_data, lane2_data],1):
        make_comparison_plots(analysis_folder, flowcell_name + "_" + str(i), lane_data, twelve_week_noNTC)
    
    return

def make_comparison_plots(analysis_folder, flowcell_name, lane_data, twelve_week_noNTC):
    #twelve_week_data = pd.read_csv('/rnd/bgould/SDTS/All_metrics_compare/Big_sample_metrics_table.csv', index_col=68)
    #twelve_week_noNTC = twelve_week_data[twelve_week_data.dbsnp_rate != "None"]
    #flowcell_data = pd.read_csv(analysis_folder + "/metrics/all_metrics.table", sep="\t")
    
    flowcell_noNTC = lane_data
    
	# MEAN COVERAGE
    plt.figure(randint(1,1000000))
    sns.distplot(twelve_week_noNTC['mean_coverage'], hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label="Production Data")
    sns.distplot(flowcell_noNTC['mean_coverage'], hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label=str(flowcell_name))
    plt.legend(prop={'size':16})
    plt.title('Mean Coverage')
    plt.xlabel('sample mean coverage depth')
    plt.ylabel('density')

    plt.savefig(analysis_folder + "/plots/" + flowcell_name + "_mean_coverage_compare.png") 

	# SD COVERAGE
    plt.figure(randint(1,1000000))
    sns.distplot(twelve_week_noNTC['sd_coverage'], hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label="Production Data")
    sns.distplot(flowcell_noNTC['sd_coverage'], hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label=str(flowcell_name))
    plt.legend(prop={'size':16})
    plt.title('SD Coverage')
    plt.xlabel('stdev of depth of sample coverage')
    plt.ylabel('density')

    plt.savefig(analysis_folder + "/plots/" + flowcell_name + "_sd_coverage_compare.png")

    # GC BIAS SLOPE
    plt.figure(randint(1,1000000))
    sns.distplot(twelve_week_noNTC['gc_bias_slope'], hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label="Production Data")
    sns.distplot(flowcell_noNTC['gc_bias_slope'], hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label=str(flowcell_name))
    plt.legend(prop={'size':16})
    plt.title('GC Bias Slope')
    plt.xlabel('GC bias slope')
    plt.ylabel('density')

    plt.savefig(analysis_folder + "/plots/" + flowcell_name + "_gc_bias_slope_compare.png")

    # RELATIVE GC CONTENT   
    plt.figure(randint(1,1000000))
    sns.distplot(twelve_week_noNTC['relative_gc_content'], hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label="Production Data")
    sns.distplot(flowcell_noNTC['relative_gc_content'], hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label=str(flowcell_name))
    plt.legend(prop={'size':16})
    plt.title('Relative GC Content')
    plt.xlabel('relative GC content')
    plt.ylabel('density')

    plt.savefig(analysis_folder + "/plots/" + flowcell_name + "_relative_gc_content_compare.png")

    # READ COUNT IQR
    #plt.figure(5)
    #sns.distplot(twelve_week_noNTC['read_count_IQR'], hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label="Production Data")
    #sns.distplot(flowcell_noNTC['read_count_IQR'], hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label=str(flowcell_name))
    #plt.legend(prop={'size':16})
    #plt.title('Read Count IQR')
    #plt.xlabel('read count IQR')
    #plt.ylabel('density')

    #plt.savefig(analysis_folder + "/plots/" + flowcell_name + "_read_count_IQR_compare.png")   

    # MAPPED READS
    plt.figure(randint(1,1000000))
    sns.distplot(twelve_week_noNTC['mapped_reads'], hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label="Production Data")
    sns.distplot(flowcell_noNTC['mapped_reads'], hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label=str(flowcell_name))
    plt.legend(prop={'size':16})
    plt.title('Mapped Reads')
    plt.xlabel('mapped reads per sample')
    plt.ylabel('density')

    plt.savefig(analysis_folder + "/plots/" + flowcell_name + "_mapped_reads_compare.png")

    #FRACTION COVERED
    plt.figure(randint(1,1000000))
    sns.distplot(twelve_week_noNTC['fraction_covered'], hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label="Production Data")
    sns.distplot(flowcell_noNTC['fraction_covered'], hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label=str(flowcell_name))
    median_fcov = float(median(flowcell_noNTC['fraction_covered']))
    plt.xlim((0.95,1.0))
    plt.legend(prop={'size':16})
    plt.title('median fraction covered: ' + str("%.2f" % float(median_fcov)))
    plt.xlabel('fraction ROI covered (>=20X) per sample')
    plt.ylabel('density')
    
    plt.savefig(analysis_folder + "/plots/" + flowcell_name + "_fraction_covered_compare.png")

    # INSERT SIZE MEDIAN
    plt.figure(randint(1,1000000))
    sns.distplot(twelve_week_noNTC['insert_size_median'], hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label="Production Data")
    sns.distplot(flowcell_noNTC['insert_size_median'], hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label=str(flowcell_name))
    #plt.xlim((0.95,1.0))
    plt.legend(prop={'size':16})
    plt.title('Insert Size Median')
    plt.xlabel('sample insert size median (bp)')
    plt.ylabel('density')

    plt.savefig(analysis_folder + "/plots/" + flowcell_name + "_insert_size_median_compare.png")
    
    # Contamination
    plt.figure(randint(1,1000000))
    sns.distplot(twelve_week_noNTC['contamination_estimate'], hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label="Production Data")
    sns.distplot(flowcell_noNTC['contamination_estimate'], hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label=str(flowcell_name))
    plt.legend(prop={'size':16})
    plt.title('contamination (percent)')
    plt.xlabel('contamination')
    plt.ylabel('density')

    plt.savefig(analysis_folder + "/plots/" + flowcell_name + "_contamination_estimate_compare.png")

    # dbSNP rate
    plt.figure(randint(1,1000000))
    sns.distplot(twelve_week_noNTC['dbsnp_rate'].apply(pd.to_numeric, errors='ignore'), hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label="Production Data")
    sns.distplot(flowcell_noNTC['dbsnp_rate'].apply(pd.to_numeric, errors='ignore'), hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label=str(flowcell_name))
    plt.legend(prop={'size':16})
    plt.title('% sample snps that are in dbSNP')
    plt.xlabel('dbSNP rate')
    plt.ylabel('density')

    plt.savefig(analysis_folder + "/plots/" + flowcell_name + "_dbsnp_rate_compare.png")

    
    print("Plots writen to " + flowcell_name + " /plots directory.")

    return 

if __name__=='__main__':
    fire.Fire(make_lane_plots)

