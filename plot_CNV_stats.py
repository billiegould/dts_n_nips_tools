"""
This script will input the cnv stats files from a processed flowcell and 
output a bar chart of CNV fast_sims sensitivity (for passing samples, >50X mean cov).
"""

import matplotlib
matplotlib.use('agg')
from matplotlib import pyplot as plt

import pandas as pd
import fire
import operator
from numpy import arange
from glob import glob

def plot_cnv_sen(analysis_dir):
    
    with open( analysis_dir + "metrics/all_metrics.table", "r") as data:
        metrics = pd.read_table(data, header=0)

    passing_samples = list(metrics.loc[metrics['mean_coverage']>= 50.0, 'identifier'])
    print passing_samples

# plot panelwide sensitivity
    with open( analysis_dir + "metrics/cnv.panelwide_sims.sample_sensitivity.csv", "r") as fp:
        lane1 = pd.read_csv(fp, header=None)    
    with open( analysis_dir + "metrics/cnv2.panelwide_sims.sample_sensitivity.csv", "r") as fp:
        lane2 = pd.read_csv(fp, header=None)
    panelwide_sen = pd.concat([lane1, lane2], join='outer', ignore_index=True)
    panelwide_sen.columns = ["sample", "sensitivity"]
    panelwide_sen = panelwide_sen.set_index('sample')

    print panelwide_sen.iloc[0:47,:]
    print panelwide_sen.iloc[47:,:]
    print panelwide_sen.shape

    panelwide_sen = panelwide_sen.loc[passing_samples, :]
    print panelwide_sen.shape
    #print panelwide_sen.iloc[0:47,:]
    #print panelwide_sen.iloc[47:,:]    

    plt.figure(1, figsize=(8,2))
    plt.title("sample panelwide CNV sensitivity")
    y_pos = arange(panelwide_sen.shape[0])
    plt.bar(y_pos, panelwide_sen.sensitivity, color='m')
    #plt.xticks(y_pos, zip(*sorted_samples)[0], rotation=45)
    #plt.rc('axes', labelsize=9)
    plt.xticks(y_pos, [])
    plt.ylim(50,100)
    plt.xlabel("samples")
    plt.ylabel("sensitivity")
    plt.axhline(75, color='r', linestyle="--")
    plt.subplots_adjust(bottom=0.2, top=0.85) 

    plt.savefig(analysis_dir + "plots/panelwide_sims.png")

# plot DMD sims sensitivity
    with open( glob(analysis_dir + "metrics/cnv.DMD_sims.sample_sensitivity.csv")[0], "r") as fp:
        lane1 = pd.read_csv(fp, header=None)
    with open( glob(analysis_dir + "metrics/cnv2.DMD_sims.sample_sensitivity.csv")[0], "r") as fp:
        lane2 = pd.read_csv(fp, header=None)
    DMD_sen = pd.concat([lane1, lane2], join='outer')
    DMD_sen.columns = ["sample", "sensitivity"]
    DMD_sen = DMD_sen.set_index('sample')
    DMD_sen = DMD_sen.loc[passing_samples, :]

    plt.figure(2, figsize=(8,2))
    plt.title("sample DMD CNV sensitivity")
    y_pos = arange(DMD_sen.shape[0])
    plt.bar(y_pos, DMD_sen.sensitivity, color='c')
    #plt.xticks(y_pos, zip(*sorted_samples)[0], rotation=45)
    #plt.rc('axes', labelsize=9)
    plt.xticks(y_pos, [])
    plt.ylim(90,100)
    plt.xlabel("samples")
    plt.ylabel("sensitivity")
    plt.axhline(99, color='r', linestyle='--')
    plt.subplots_adjust(bottom=0.2, top=0.85)
   
    plt.savefig(analysis_dir + "plots/DMD_sensitivity.png")

# plot CFTR sims sensitivity
    with open( analysis_dir + "metrics/cnv.CFTR_sims.sample_sensitivity.csv", "r") as fp:
        lane1 = pd.read_csv(fp, header=None)
    with open( analysis_dir + "metrics/cnv2.CFTR_sims.sample_sensitivity.csv", "r") as fp:
        lane2 = pd.read_csv(fp, header=None)
    CFTR_sen = pd.concat([lane1, lane2], join='outer')
    CFTR_sen.columns = ["sample", "sensitivity"]
    CFTR_sen = CFTR_sen.set_index('sample')
    CFTR_sen = CFTR_sen.loc[passing_samples, :]

    plt.figure(3, figsize=(8,2))
    plt.title("sample CFTR CNV sensitivity")
    y_pos = arange(CFTR_sen.shape[0])
    plt.bar(y_pos, CFTR_sen.sensitivity, color='y')
    #plt.xticks(y_pos, zip(*sorted_samples)[0], rotation=45)
    #plt.rc('axes', labelsize=9)
    plt.xticks(y_pos, [])
    plt.ylim(90,100)
    plt.xlabel("samples")
    plt.ylabel("sensitivity")
    plt.axhline(99, color='r', linestyle='--')
    plt.subplots_adjust(bottom=0.2, top=0.85)

    plt.savefig(analysis_dir + "plots/CFTR_sensitivity.png")


    print("Sample CNV plots complete")

if __name__=="__main__":
    fire.Fire(plot_cnv_sen)
