#!/usr/bin/env python

import os
import fire
import pysam
import IPython
import itertools
import datetime
from collections import Counter
import numpy
import gc

from statsmodels.nonparametric.smoothers_lowess import lowess

import matplotlib
matplotlib.use("Agg")
#matplotlib.rcParams['agg.path.chunksize'] = 10000   # for very large plots, speeds up and prevents plottting hang error
from matplotlib import pyplot as plt

#from PIL import Image, ImageDraw

from hmmlearn import hmm
import warnings
warnings.simplefilter("ignore")

#print "TEST"
#from IPython import embed
#embed()



# group adjacent values in a sorted list
def one_d_cluster(v, win_size):     # input vector of bp positions of called cnv window midpoints
    dist = 3 * win_size             # an adjustable parameter, how many BASES apart do high confidence window midpoint values have to be to be part of one cnv call?
    clusters = []
    current = []
    for i in range(1,len(v)):
        if (v[i] - dist) <= v[i-1]:        # if a call is at least this many bp from the previous call
                                              #start the first cluster and/or append position to current cluster
            current.append(v[i])
        else:                           #if call not near the last call,  end current cluster and start a new one
            clusters.append(current)
            current = [v[i]]         
    
    if len(current) > 0:             # get the last cluster
        clusters.append(current)

    clusterbounds = [(min(c), max(c)) for c in clusters]
    return clusterbounds


# Call CNVs in continuous sequencing data
# Predicts the copy number (equivalent to normalized read depth 0.5, 1.0, or 1.5)  at each base (the hidden state) given observed depth and optimal model (transition) parameters.
# If state is predicted as 0.5, call deletion, if 1.5 call dup. Cluster called bases into single calls

def cnv_hmm(trace, bam, region, output_prefix, embed=False, plot_out=None, breakreads=False):              # can't actually use embed=TRUE here when using multiprocessing 
    model = hmm.GaussianHMM(n_components=3,
                            params="mct", # stmc are the choices
                            init_params="c",
                            tol=0.0001)         # tol is max amt change observed for parameter estimation EM iteration to stop

    kernel_size = 30000
    kernel = [1.0 / kernel_size] * kernel_size              # vector len k each element 1/k
    trace = numpy.convolve(trace, kernel, mode="same")    # linear discrete convolution: sum of sliding window products

    print "one"
    for i in range(len(trace)):
        if trace[i] < 0.8:
            trace[i] = 0.0
        else:
            break

    for i in list(reversed(range(len(trace)))):
        if trace[i] < 0.8:
            trace[i] = 0.0
        else:
            break
    sp = 0.7
    model.startprob_ = numpy.array([(1-sp)/2, sp, (1-sp)/2])
    model.transmat_ = numpy.array([[sp, (1-sp)/2, (1-sp)/2],
                                   [(1-sp)/2, sp, (1-sp)/2],
                                   [(1-sp)/2, (1-sp)/2, sp]])

    model.means_ = numpy.array([[0.5], [1.0], [1.5]])
    model.covars_ = numpy.tile(numpy.identity(1), (3, 1, 1))
    #X, Z = model.sample(100)

    X = trace.reshape(-1,1)   #converts one row array to one column array (X = normalized, smoothed, read depth per base)
    model.fit(X)                # determine HMM parameters using EM
    print "two"
    logprob, states = model.decode(X)       # why is this line here?
    state_probs = model.predict_proba(X)    # # predict the probability of each hidden state at each (smoothed) base

    log_state_probs = numpy.log10(state_probs)

    log_cn1_conf = log_state_probs[:,0] - numpy.maximum(log_state_probs[:,1], log_state_probs[:,2])  # confidence of deletion at each base
    log_cn3_conf = log_state_probs[:,2] - numpy.maximum(log_state_probs[:,1], log_state_probs[:,0])     # confidence of duplication at each base

    log_cn1_conf[trace == 0] = 0        # removes null confidence values at 0 depth bases
    log_cn3_conf[trace == 0] = 0

    # a code block for putting out a file ond plot of windowed cnv confidence levels
    filename = output_prefix + os.path.basename(bam) + "_win_logconf.txt"
    with open(filename, "w+") as windowfile:
        left = 0                                #use relative coordinates to start
        cn1_win_vals=[]
        cn3_win_vals=[]
        win_mids=[]
        win_size = 1000
        windowfile.write("position\tcn1_log_conf\tcn3_log_conf\tfile\n")
        for i in range(len(trace)/win_size):        
            right = left + win_size
            win_cnf1 = numpy.mean(log_cn1_conf[left:right])
            win_cnf3 = numpy.mean(log_cn3_conf[left:right])
            win_mid = numpy.mean([ right + int(region[1]), left + int(region[1]) ])  #record window midpoints using global genomic position
            windowfile.write("%s\t%s\t%s\t%s\n" % (str(win_mid), str(win_cnf1), str(win_cnf3), str(os.path.basename(bam))))
            win_mids.append(win_mid)
            cn1_win_vals.append(win_cnf1)
            cn3_win_vals.append(win_cnf3)
            left = right

    #smooth the windowed call confidence values:
    cn1_win_vals_smooth = numpy.array(lowess(cn1_win_vals, win_mids, frac=0.1, return_sorted=False))   # this returns the loess predicted values as the x positions  
    cn3_win_vals_smooth = numpy.array(lowess(cn3_win_vals, win_mids, frac=0.1, return_sorted=False))
    
    win_mids = numpy.array(win_mids)
    cn1_win_vals = numpy.array(cn1_win_vals)
    cn3_win_vals = numpy.array(cn3_win_vals)

        # could output a single sample boxplot here . . . . 
    threshold = 15.0
    cn1_calls = one_d_cluster( win_mids[numpy.where(cn1_win_vals_smooth > threshold)[0]], win_size)   # cluster the BASE POSITIONS of high confidence windows    
    cn3_calls = one_d_cluster( win_mids[numpy.where(cn3_win_vals_smooth > threshold)[0]], win_size)
    print cn1_calls, bam

    # report only calls in the reduced region (non-overlapping part of the sliding window for the bam). Include CNV only if > 50% of length is in the reduced region
#    for start, stop in cn1_calls:
#        if (stop - start)/2.0 < 5000:
#            cn1_calls.remove((start,stop))   
#        elif (stop - start)/2.0 > (len(log_cn1_conf)-5000):
#            cn1_calls.remove((start,stop))

#    for start, stop in cn3_calls:/
#        if (stop - start)/2.0 < 5000:
#            cn3_calls.remove((start,stop))
#        elif (stop - start)/2.0 > (len(log_cn3_conf)-5000):
#            cn3_calls.remove((start,stop))
    print "three"

#    Require calls to start or stop more than 1000bp from the edge of the trace
#    Require calls to start or stop more than 1000bp from the edge of the trace
    cn1_calls = [(start, stop) for start, stop in cn1_calls
                 if (start > 1000) and (stop < (int(region[2]) - 1000))]

    cn3_calls = [(start, stop) for start, stop in cn3_calls
                 if (start > 1000) and (stop < (int(region[2]) - 1000))]

    print "four"
    # Require calls to be longer than 3 times the window size
    cn1_calls = [(start, stop) for start, stop in cn1_calls
                 if (stop - start >= (3*win_size))]

    cn3_calls = [(start, stop) for start, stop in cn3_calls
                 if (stop - start >= (3*win_size))]
    print cn1_calls, bam
    print "five"
    # Extract average posterior confidence for each call
    print str( 33819500.0 in win_mids)
    
    cn1_calls = [(start, stop, cn1_win_vals[ numpy.where(win_mids==start)[0]:(numpy.where(win_mids==stop)[0]+1) ].mean()) for start, stop in cn1_calls]
    
    cn3_calls = [(start, stop, cn3_win_vals[ numpy.where(win_mids==start)[0]:(numpy.where(win_mids==stop)[0]+1) ].mean()) for start, stop in cn3_calls]
    
    print cn1_calls, bam
    print "six"

    cn1_breakreads = ["NA", "NA", "NA", "NA"]  # normally returns  [ list_discords, len_discord_inserts, list_splits?, len_splits? ]
    cn3_breakreads = ["NA", "NA", "NA", "NA"]
    if breakreads:
        cn1_breakreads = find_breakpoint_reads(bam, region, cn1_calls, output_prefix, splits=True) 
        cn3_breakreads = find_breakpoint_reads(bam, region, cn3_calls, output_prefix, splits=False)
    print "seven"
    
    # Plot calls because it's fun
    if plot_out is not None:
        plt.figure(figsize=(30,3), dpi=72)            # for long plots
        plt.ylim(0, 2.0)
        plt.plot(range(region[1], region[2]), trace[:], "bo", alpha=0.25)
#        plt.plot(range(region[1], region[2]), trace[:] * (log_cn1_conf[:] > threshold), "ro", alpha=0.25)
#        plt.plot(range(region[1], region[2]), trace[:] * (log_cn3_conf[:] > threshold), "yo", alpha=0.25)
        for start, stop, conf in cn1_calls:
            plt.plot([start,stop], [0.2,0.2], color="red", linewidth=3)

        for start, stop, conf in cn3_calls:
            plt.plot([start,stop], [1.9,1.9], color="blue", linewidth=3)
 
        plt.grid()
        print "eight"
        if breakreads:
            for cnv1 in cn_1_breakreads:
                y = 0.19
                mates = cnv1[2]
                for pair in mates:                
                    y -= 0.01
                    plt.plot( pair, [y,y], color='r', linestyle='-')
                s_reads = cnv1[3]
                for s in s_reads:               
                    y += -0.01
                    plt.plot(s, y, "ro")
        
            for cnv3 in cn_3_breakreads:
                y = 1.91
                mates = cnv3[2]
                for pair in mates:
                    y += 0.01
                    plt.plot( pair, [y,y], color='y', linestyle='-' )
        
        plt.legend(["CN=2", "CN=1", "CN=3"], loc=4)
        plt.savefig(plot_out, format='PNG')
        plt.close()
        
    # Drop into a shell if need be
    if embed:
        from IPython import embed; embed()

    
    #adjust the call coordinates to genomic coords
#    cn1_calls = [ (conf, start + region[1], stop + region[1]) for conf,start,stop in cn1_calls ]
#    cn3_calls = [ (conf, start + region[1], stop + region[1]) for conf,start,stop in cn3_calls ]
    print "nine"
    
    # Fin!
    return cn1_calls, cn3_calls, model, logprob, states, state_probs, cn1_breakreads, cn3_breakreads

def _cnv_hmm(args):
    return cnv_hmm(args[0], plot_out=args[1], bam=args[2], region=args[3], output_prefix=args[4], breakreads=False)

# Load continuous sequencing data, compute coverage trace, and normalize
def coverage_trace(bam, region):

    trace = []

    bam_fp = pysam.AlignmentFile(bam)

    chrom, start, stop = region
    tr = numpy.array([0.0] * (stop - start))
    for read in bam_fp.fetch(chrom, start, stop):
        pos = read.pos
#        read_length = len(read.seq)
        if (pos < start) or (pos > stop):
            continue

           # if scipy.stats.uniform.rvs() > downsample_factor:
           #     continue

        tr[pos - start] += 1

    trace.extend(list(tr))

    trace = numpy.array(trace)

    k = 500
   
    smooth_coverage_trace = numpy.convolve(trace,
                                           numpy.array([1.0] * k) / k,
                                           mode="same")

   # norm_coverage_trace = smooth_coverage_trace / (numpy.median(smooth_coverage_trace) + 1e-9) 
    norm_coverage_trace = smooth_coverage_trace / (numpy.mean(smooth_coverage_trace))               # why would you use median that is influenced by outliers? also, if the median of the trace is zero, something is wrong and it really should throw a divide by zero error
  
    bam_fp.close()

#    return trace, smooth_coverage_trace, norm_coverage_trace
    return norm_coverage_trace

# Wrapper for parallel map
def _coverage_trace(args):

    return coverage_trace(*args)

# Handy plot
def plot_results(trace, calls, output):
    plt.plot(trace, ".", alpha=0.2)
    plt.plot(calls, ".", alpha=0.2)
    plt.grid()
    plt.legend(["smoothed trace", "calls"])
    plt.savefig(output)
    plt.close()

# Load a bed file into list of (chrom, start, stop) tuples
def load_bed(bed_path):
    regions = []
    with open(bed_path, "r") as bed:
        for line in bed:
            r = line.rstrip().split("\t")
            regions.append((r[0], int(r[1]), int(r[2])))
    return regions

# Plot all traces
def plot_traces(traces, output):
    plt.figure(figsize=(30,3), dpi=72)        # for long plots
    for i in range(traces.shape[0]): 
       plt.plot(traces[i,:], "-", alpha=0.5)
    plt.ylim((0,4))
    plt.grid()
    plt.savefig(output)
    plt.close()

# Do the thing
def load_traces(bam_path, bed_path, output_prefix, region, ref_path="/rnd/genomics/genomes/hg19/hg19.fa"):
    # Handle the input arguments
    reference = pysam.FastaFile(ref_path)

    if bam_path.endswith(".bam"):
        bams = [bam_path]
    else:
        with open(bam_path) as fp:
            bams = [line.strip() for line in fp]

    #roi_size = 0
#    chrom, start, stop = exp_region
#    roi_size = stop - start

    # Compile bam-reading instructions for parallel map
    instructions = []
    for bam in bams:
            #ct, s_ct, n_ct = coverage_trace(reference, bam, chrom, start, stop)
         instructions.append((bam, region))

    
#    from IPython import embed; embed()

    # Read all the bams, generate coverage traces, and normalize them (within sample)
    from multiprocessing import Pool
    pool = Pool(processes=18)
    results = pool.map(_coverage_trace, instructions)
    
# close the worker pool to prevent results from going into memory for the duration of the script
    pool.close()
    pool.join()

#    traces = numpy.array([r[2] for r in results])
    traces = numpy.array(results)

    # Squash the bad sites
    traces[:,traces.mean(axis=0) < 0.2] = 0

    return traces


# bam is a bam file name, calls are list of [(conf, start, stop)]
def find_breakpoint_reads(bam_file, region, calls, output_prefix, splits=None):
    
    # construct coordinate map
    locs = []
    chrom_1, start, stop = region           # set up for reading cnvs from a single chrom only here
    for j in range(stop - start):
        locs.append((start + j))
    #print "A"
    
    # for each cnv...
    results = []
    w = 5000            # the window size in which to look for breakpoint reads
    with pysam.AlignmentFile(bam_file) as bam:
        for conf, start, stop in calls:
            #print str( (conf, start, stop) )
            pos_1 = locs[start]    #the left breakpoint genomic coord.
            pos_2 = locs[stop]     #the right breakpoint genomic coord.

            start_1 = pos_1 - w         #left side of window 1 to look for reads in
            stop_1 = pos_1 + w          #right side of window 1 to look for reads in

            start_2 = pos_2 - w         #window sides for searching aroudn breakpoint 2
            stop_2 = pos_2 + w

            reads = []
            d_read_info = []
            s_read_info = []
     #       print "B"
#            print "left: " + chrom_1 + " " + str(start_1) + " " + str(stop_1)
#            print "right: " + chrom_1 + " " + str(start_2) + " " + str(stop_2) 
            for read in bam.fetch(chrom_1, start_1, stop_1):        # look for break reads around left brkpt
                if read.is_paired:
                    mate_chrom = read.next_reference_name
                    mate_pos = read.next_reference_start
                    if mate_chrom == chrom_1 and numpy.abs(mate_pos - pos_2) < w:  #if the mate start position is within 50kb of the right breakpoint
                        reads.append(read)
                        if read.pos < locs[-1] and mate_pos < locs[-1]:
                            info = (read.pos, mate_pos)   #currently does not include reads that extend beyond region
                            d_read_info.append(info)
                
                if splits == True and read.has_tag("SA"):
                    second_aln = read.get_tag("SA").split(",")
                    s_chrom = second_aln[0]
                    s_pos = second_aln[1]
                    if s_chrom == chrom_1 and numpy.abs(int(s_pos) - pos_2) < w:
                       # print read
                        info = (read.pos)
                        s_read_info.append(info)
            #print "D"
            for read in bam.fetch(chrom_1, start_2, stop_2):        # look for break reads around the right brkpt
                if read.is_paired:
                    mate_chrom = read.next_reference_name
                    mate_pos = read.next_reference_start
                    if mate_chrom == chrom_1 and numpy.abs(mate_pos - pos_1) < w:
                        reads.append(read)
                        if read.pos < locs[-1] and mate_pos < locs[-1] and (mate_pos, read.pos) not in d_read_info:
                            info = (read.pos, mate_pos)
                            d_read_info.append(info)

                if splits == True and read.has_tag("SA"):
                    second_aln = read.get_tag("SA").split(",")
                    s_chrom = second_aln[0]
                    s_pos = second_aln[1]
                    if s_chrom == chrom_1 and numpy.abs(int(s_pos) - pos_1) < w:
              #          print read
                        info = (read.pos)
                        s_read_info.append(info)
            #print d_read_info
            #/print s_read_info
            #print "E"
            insert_avg = numpy.abs([r.mpos - r.pos for r in reads]).mean()

            results.append((len(reads), insert_avg, d_read_info, s_read_info))
    
    return results


def process_traces(bams, region, traces, output_prefix):

    # Normalize across samples
    n_traces = traces / (numpy.median(traces, axis=0) + 1e-9)

#    n_traces = traces           # use this if only processing one sample

    bad_sites  = numpy.array(numpy.abs(1.0 - n_traces.mean(axis=0)) > 0.2)
    bad_sites = numpy.array(n_traces.std(axis=0) > 5.0)

    n_traces[:,bad_sites] = 0

#    region = exp_region[0], (exp_region[1]+5000), (exp_region[2]-5000)

    # Find CNVs
#    html = open(os.path.dirname(output_prefix) + "/index.html", "w+")

    with open( output_prefix + ".calls", "a+") as fp:
        print >> fp, "copy_number conf start stop cnv_len n_brpt_reads brpt_read_len n_split_reads bam_name"
    fp.close()

    html = open( output_prefix + "_" + ("%s_%d-%d" % region) + ".html", "a+")
    # print >> html, "copy_number : conf : start : stop : cnv_len : n_brpt_reads : brpt_read_len : n_split_reads : bam_name", "<br/>"

    names = [ os.path.splitext(os.path.basename(bam))[0] for bam in bams ]

    with open( output_prefix + ".calls", "a+") as fp:
        from multiprocessing import Pool
        pool = Pool(processes=18)
        args = [(n_traces[i,:], names[i] + "_" + ("%s_%d-%d" % region) + ".trace.png", bams[i], region, output_prefix) for i in range(len(bams))]
        results = pool.map(_cnv_hmm, args)

        pool.close()
        pool.join()
# This replaced with attachment to output from cnv_hmm output results
#        cn1_breakpoints = [None] * len(bams)
#        cn3_breakpoints = [None] * len(bams)
#        for sample_idx in range(len(bams)):
#            bam_name = bams[sample_idx]
#            cn1_calls, cn3_calls, m, p, s, sp = results[sample_idx]
#           cn1_breakpoints[sample_idx] = find_breakpoint_reads(bam_name, regions, cn1_calls, output_prefix, splits=True)
#           cn3_breakpoints[sample_idx] = find_breakpoint_reads(bam_name, regions, cn3_calls, output_prefix, splits=False)

#            for sample_idx in range(len(bams)):
#                bam_name = bams[sample_idx]

        for sample_idx in range(len(results)):
            name = names[sample_idx]
            print >> html, name, "<br/>"
            print >> html, "<img src=\"%s\"><br/>" % (name + "_" + ("%s_%d-%d" % region) +".trace.png")
            cn1_calls, cn3_calls, m, p, s, sp, cn_1_breakreads, cn_3_breakreads  = results[sample_idx]
      
        # add the data for individual bam to the compiled html ouptu for the region
        
            if (len(cn1_calls) + len(cn3_calls)) == 0:
                continue
            
            # print to fp (outprefix.calls), includes calls for all regions
            for i in range(len(cn1_calls)):
                start, stop, conf = cn1_calls[i]
#                n_breakpoint_reads, breakpoint_read_length = results[sample_idx][6][i][0:2]
#                n_split_reads = len(results[sample_idx][6][i][2])/2
#                print >> fp, 1, conf, start, stop, stop - start, n_breakpoint_reads, breakpoint_read_length, n_split_reads, name
                print >> fp, 1, start, stop, conf, stop - start, name
                print >> html, 1, start, stop, conf, stop - start, name, "<br/>"

            for i in range(len(cn3_calls)):
                conf, start, stop = cn3_calls[i]
#                n_breakpoint_reads, breakpoint_read_length = results[sample_idx][7][i][0:2]
#                print >> fp, 3, conf, start, stop, stop - start, n_breakpoint_reads, breakpoint_read_length, "NA", name
                print >> fp, 3, conf, start, stop, stop - start, name
                print >> html, 3, conf, start, stop, stop - start, name, "<br/>"
            
                #print >> html, 3, conf, start, stop, stop - start, n_breakpoint_reads, brepoint_read_length, "NA", "<br/>"
        html.close()

#    from IPython import embed; embed()


def main(bam_path, bed_path, output_prefix, ref_path="/rnd/genomics/genomes/hg19/hg19.fa"):
    if bam_path.endswith(".bam"):
        bams = [bam_path]
    else:
        with open(bam_path) as fp:
            bams = [line.strip() for line in fp]

    regions = load_bed(bed_path)

    for region in regions:   
        reg_name = "%s_%d-%d" % region

        names = [ os.path.splitext(os.path.basename(bam))[0] for bam in bams ]

        if os.path.exists(names[0] + "_" + ("%s_%d-%d" % region) +".trace.png"):
            continue 
        
        # load or create coverage traces for the window   
        if not os.path.exists(output_prefix + "_" + reg_name + ".traces.npy"):
            traces = load_traces(bam_path, bed_path, output_prefix, region, ref_path)
            numpy.save(output_prefix + "_" + reg_name + ".traces.npy", traces)
        else:
            traces = numpy.load(output_prefix + "_" + reg_name + ".traces.npy")
        
        process_traces(bams, region, traces, output_prefix)  # this function also writes out results to html and samples.calls file
        with open( output_prefix + "_" + reg_name + ".html", "a+") as html:
            plot_traces(traces, (output_prefix + "_" + reg_name + ".traces.png"))
            print >> html, "<img src=\"%s\" > <br/>" % (output_prefix + "_" + reg_name + ".traces.png")
        html.close()
        
        # delete the traces for the region (only) and run garbage collection to free memory
        del traces
        gc.collect()

if __name__ == '__main__':
    fire.Fire(main)
