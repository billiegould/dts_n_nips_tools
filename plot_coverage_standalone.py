import sys
#sys.path.append("~/Desktop/seqpipeline.dev/seq_pipeline/")      # set path to local dev/seq_pipeline repository

from collections import defaultdict
import cPickle
#import _pickle as cPickle  # use this for running with python3
import json
import math
import logging
from os.path import join, exists

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import numpy as np
from itertools import groupby
from glob import glob

try:
    from seq_pipeline.command import command
except ImportError:
    print("ERROR: Make sure you have the /master/seqpipeline environment installed OR add the path to dev/seqpipeline to your sys.path.")
    sys.exit()

from seq_pipeline.command import NullIntArg
from seq_pipeline.command import SequencingBatchArg
from seq_pipeline.metrics.coverage_qc import is_empty
from seq_pipeline.utils.interval_list import IntervalList
from seq_pipeline.utils.pathutils import savefig
from seq_pipeline import settings

from seq_pipeline.metrics import coverage_qc
from seq_pipeline.metrics import plot_coverage as pc


SKIP_SAMPLES = ['NTC']
#BAD_ROI_THRESH = 10                  #adjust, default = 10 in seqpipeline
MAX_YLIM = 1000
MAX_COVG_PANELS_PER_FIGURE = 100
COVERAGE_THRESHOLDS = 50             # adjust, defaults to settings.coverage_thresholds in seqpipeline. avg base depth below which to flag samples

if __name__ == "__main__":
    from argparse import ArgumentParser, SUPPRESS
    parser = ArgumentParser(add_help=False)

    parser.add_argument("-h", "--help", action='help', default=SUPPRESS, help='This script will run the functions in plot_coverages.py from seq_pipeline using specified input parameters. Output plots show coverage depth for all samples \
                                and highlight in red areas where the minimum coverage sample is below the specified BAD_ROI_THRESH. Edit this script to specify the path to your local seq_pipeline repository before running. Run in parallel with one thread per sample, 2G memory per thread')

    parser.add_argument("-s", "--sample_list", dest="samples", default="", help="text file containing sample identifiers (barcodes: s_lane#_barcode), prefixes to coverage.pkl files to process. \
                                Remember to remove the NTC samples. \
                                Try: for file in *.pkl; do NAME=$(basename $file .coverage.pkl); echo $NAME; done > samples.list")

    parser.add_argument("-r", "--reportable_roi", dest="reportable_roi_bed", default="", help="reportable_roi.bed file")

    parser.add_argument("-w", "--workdir", dest="workdir", default=".", help="the analysis directory where the metrics folder is located. also used as the output directory")

    parser.add_argument("-n", "--outprefix", dest="outprefix", default="", help="output prefix for plot file names, usually a flowcell barcode name (runname)")

    parser.add_argument("-c", "--bad_roi_threshold", dest="bad_roi_thresh", default=0, help="min. sample coverage below which to highlight for each gene roi a failure ", type=int)

    parser.add_argument("--calc_failrates_only", dest="calc_failrates_only", default=False, help="do not create plots. only output per roi fraction covered > /metrics/gene_rois_below_20x.frac_cov.txt")     

    args = parser.parse_args()


    with open(args.workdir + "/alignments/" + args.samples, 'r') as sample_file:
        identifiers = []
        for line in sample_file:
            identifiers.append(line.strip())
    print("Processing samples: %s \n" % identifiers)

    metrics_dir = args.workdir + "/metrics/"

    roi_interval_list = IntervalList(args.reportable_roi_bed, 0)

    if not args.calc_failrates_only:
        # if coverage pkl files do not exist, create them:
        needs_coverage_pkl = []
        for sample in identifiers: 
            if len( glob( metrics_dir + sample + "*coverage.pkl")) == 0:
                needs_coverage_pkl.append(sample)  
    
        if len(needs_coverage_pkl) >0:
            print("Computing coverage for samples: " + str(needs_coverage_pkl))
            from multiprocessing import Pool
            pool = Pool(24)
            #aln = glob(args.workdir + "/alignments/" + sample + "*final.bam")[0]
            results = [ (sample, pool.apply_async(coverage_qc.compute_coverage, (glob(args.workdir + "/alignments/" + sample + "*final.bam")[0], roi_interval_list))) for sample in needs_coverage_pkl ]
            for sample, r in results:      
                data = r.get()
                with open( (metrics_dir + sample + ".coverage.pkl"), "wb") as fp:
                    cPickle.dump( data, fp ) 
            pool.close()
            pool.join()

        # Load the pickle coverage files together:
        print("Using previous coverage_pkl files. If your gene ROIs are not in these files, delete old coverage_pkl files before running this script.")
        coverage_data = pc.create_coverage_data(identifiers, metrics_dir)

        #from IPython import embed; embed()
        low_samples = {}
        all_sample_coverages={}
        num_rois = len(coverage_data.keys())
        for sample in identifiers:
            sample_coverage = 0
            for roi in coverage_data.keys():
                #print("ROI:", str(roi))
                sample_coverage += np.mean(coverage_data[roi][sample])
        
            avg_sample_roi_cov = float(sample_coverage)/num_rois
            all_sample_coverages[sample]= avg_sample_roi_cov
            
            if avg_sample_roi_cov < COVERAGE_THRESHOLDS:
                low_samples[sample] = avg_sample_roi_cov

        sys.stderr.write("Average Sample Gene ROI Coverages: \n %s \n\n" % str(all_sample_coverages))

        if len(low_samples.keys()) > 0:
            sys.stderr.write("WARNING: Removing low coverage or NTC samples: coverages: " + str(low_samples) )

        passing_samples = [i for i in identifiers if i not in low_samples.keys()]

        #from IPython import embed; embed()
        print("Passing samples: ", passing_samples)    

        pc.plot_all_gene_coverages(coverage_data, roi_interval_list, passing_samples, pl_name="research_plot", workdir=args.workdir, runname=args.outprefix, coverage_threshold=args.bad_roi_thresh)

        print("\n Plots written to plots directory.\n")


    # Calculate gene fraction covered and output to file
    #read in gene roi sizes:
    gene_sizes=defaultdict(list)
    with open(args.reportable_roi_bed, "r") as roifile:
        for line in roifile:
            tile = line.split()[3]
            gene = tile.split("_")[0]
            start = int(line.split()[1])
            end = int(line.split()[2])
            gene_sizes[gene].append(end - start)
        print(gene_sizes['SeqAllele:428872'])
    #read in low coverage regions
    low_cov_regions = defaultdict(list)
    with open(args.workdir + "metrics/gene_rois_below_" + str(args.bad_roi_thresh) + "x." + str(args.outprefix) + ".bed", "r") as failing_regions:
        for line in failing_regions:
            gene = line.split()[3]
            start = int(line.split()[1])
            end = int(line.split()[2])
            low_cov_regions[gene].append(end-start)
        print(low_cov_regions['SeqAllele:428872'])
    #calculate fraction covered
    with open(args.workdir + "metrics/gene_rois_below_" + str(args.bad_roi_thresh) + "x." + str(args.outprefix) + ".95frac_cov.txt", "w") as outfile: 
        for gene, tiles in gene_sizes.iteritems():
            frac_cov = 1 - sum(low_cov_regions[gene])/float(sum(tiles))
            if frac_cov <= 0.950:
                outfile.write(gene + "\t" + str("%.4f" % frac_cov) + "\n")
    print("Stats written to metrics/gene_rois_below_" + str(args.bad_roi_thresh) + "x." + str(args.outprefix) + ".95frac_cov.txt")
