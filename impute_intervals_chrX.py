import sys
import os
from multiprocessing.pool import Pool



def impute_interval(interval, bounds):
	cmd = ('./impute_v2.3.2_x86_64_dynamic/impute2 -m 1000GP_Phase3/genetic_map_chrX_nonPAR_combined_b37.txt '
		'-h 1000GP_Phase3/1000GP_Phase3_chrX_NONPAR.hap -l 1000GP_Phase3/1000GP_Phase3_chrX_NONPAR.legend '
		'-g phg000898.v1.NCI_OncoArray_DRIVE.genotype-calls-matrixfmt.c8/SET{setno}/{chrom}_SET{setno}.ped.gen '
		'-strand_g 1000GP_Phase3/{chrom}_strand_info.txt -align_by_maf_g_ref '
		'-int {left} {right} -k_hap 800 -Ne 20000 '
		'-o phg000898.v1.NCI_OncoArray_DRIVE.genotype-calls-matrixfmt.c8/SET{setno}/{chrom}_SET{setno}_{interval}.impute2')

	fcmd = cmd.format(chrom = chrom, setno = setno, left = bounds[0], right = bounds[1], interval = interval)
	os.system(fcmd)
	return


if __name__ == "__main__":
	
	setno = sys.argv[1]
	chrom = str(os.environ['SGE_TASK_ID'])


	interval_dict = {}
	int_no=0

	with open("phg000898.v1.NCI_OncoArray_DRIVE.genotype-calls-matrixfmt.c8/" + chrom + "_Intervals.txt", "r") as intervals:
		for line in intervals:
			int_no += 1
			start = line.split()[0]
			end = line.split()[1]
			interval_dict[int_no] = (start,end)
	
	nprocs = 5
	pool = Pool(nprocs)
	completed = 0
	for interval,bounds in interval_dict.items():
		pool.apply_async(impute_interval, args=(interval, bounds))
		completed += 1

	pool.close()
	pool.join()

	assert completed == int_no  # check that all intervals completed


	cmd = ('cat phg000898.v1.NCI_OncoArray_DRIVE.genotype-calls-matrixfmt.c8/SET{setno}/{chrom}_SET{setno}_*.impute2 > phg000898.v1.NCI_OncoArray_DRIVE.genotype-calls-matrixfmt.c8/SET{setno}/{chrom}_SET{setno}_All_impute2 '
		'&& rm phg000898.v1.NCI_OncoArray_DRIVE.genotype-calls-matrixfmt.c8/SET{setno}/{chrom}_SET{setno}_*.impute2 '
		'&& mv phg000898.v1.NCI_OncoArray_DRIVE.genotype-calls-matrixfmt.c8/SET{setno}/{chrom}_SET{setno}_*.impute2_* phg000898.v1.NCI_OncoArray_DRIVE.genotype-calls-matrixfmt.c8/SET{setno}/{chrom}_logs/ ')

	fcmd = cmd.format(setno = setno, chrom = chrom)
	os.system(fcmd)

	print("Completed SET " + setno + " Chrom " + chrom )

