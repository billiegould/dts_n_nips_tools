# this script requires python3

import pandas as pd
import fire
import yaml
import tabulate
import stor
import os
import re
from numpy import inf

def make_fc_rst_report(template_yaml, workdir, docdir, flowcell_name, super_toc_file):
    append_to_toc_tree(super_toc_file, f'{flowcell_name}/{flowcell_name}')
    RstReport(template_yaml, workdir, docdir, flowcell_name, super_toc_file)


class RstReport(object):
    def __init__(self, template_yaml, workdir, docdir, flowcell_name, super_toc_file):
        self.template = yaml.load(open(template_yaml, 'r'))
        self.workdir = workdir
        self.flowcell_name = flowcell_name
        self.outdir = ifnotexistmkdir(f'{docdir}/{flowcell_name}')
        self.plotdir = ifnotexistmkdir(f'{self.outdir}/plots')

        function_lookup = {'h1': self.make_header1,
                           'h2': self.make_header2,
                           'h3': self.make_header3,
                           'text': self.make_text,
                           'fig': self.make_figure,
                           'table': self.make_table}
        self.make_toc()

        for section, contents_list in self.template['SECTIONS'].items():
            text = ''
            for (f_key, content) in contents_list:
                text += function_lookup[f_key](content)

            with open(f'{self.outdir}/{section}.rst', 'w') as outfile:
                outfile.write(text)


    def make_text(self, text):
        return text + "\n\n\n"

    def make_header1(self, text):
        ul = ''.join(['=' for l in text])
        return f'{text}\n{ul}\n'

    def make_header2(self, text):
        ul = ''.join(['-' for l in text])
        return f'{text}\n{ul}\n'

    def make_header3(self, text):
        ul = ''.join(['^' for l in text])
        return f'{text}\n{ul}\n'

    def make_table(self, rel_path):
        this_table = f'{self.workdir}/{rel_path}'
        df = pd.read_csv(this_table, index_col=0)
        return tabulate.tabulate(df, headers=df.columns, tablefmt='rst') + "\n\n\n"

    def make_figure(self, data):
        rel_path, fig_name = data
        img_path = f'{self.workdir}/{rel_path}'
        basename = os.path.basename(img_path)
        dest_path = f'{self.plotdir}/{basename}'
        stor.copy(img_path, dest_path)

        text = self.make_header3(fig_name)
        text += f'.. figure:: plots/{basename}\n'
        text += '\t:width: 1000px\n'
        text += '\t:align: center\n\n'
        return text

    def make_toc(self):
        text = self.make_header1(self.flowcell_name)
        text += ".. toctree::\n\t:maxdepth: 2\n\n"
        for section in self.template['SECTIONS'].keys():
            text += f"\t{section}\n"
        with open(f'{self.outdir}/{self.flowcell_name}.rst', 'w') as outfile:
            outfile.write(text)


def append_to_toc_tree(toc_path, new_section):
    toc_file_lines = []
    sectionliststart = inf
    new_section = new_section
    existing_sections = []
    with open(toc_path, "r") as toc_file:
        for i, line in enumerate(toc_file):
            if "maxdepth" in line:
                sectionliststart = i + 1
            if i > sectionliststart:
                existing_sections.append(line.strip())
                if not re.match(r'\s+\w+', line) and new_section not in existing_sections:
                    toc_file_lines.append("   " + new_section)
                    sectionliststart = inf
            toc_file_lines.append(line.rstrip())

    with open(toc_path, "w") as toc_file:
        toc_file.write("\n".join(toc_file_lines))
        toc_file.write("\n")


def ifnotexistmkdir(directory):
    '''
    If directory doesn't exist make it

    Args:
        directory (str): path to directory to check

    Retruns:
        str: directory returned
    '''
    if not os.path.exists(directory):
        os.makedirs(directory)
    return directory


if __name__ == '__main__':
    fire.Fire(make_fc_rst_report)
