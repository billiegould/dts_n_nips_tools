
# this script will plot the data from a flowcell against metrics files gathered fromt eh last 12 weeks fo produciton runsi


import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt

from numpy import median
import seaborn as sns
import pandas as pd
import fire
import os



def calc_insert_sizes(analysis_folder):
    """
    Some versions of seq pipeline do not seem to output median insert size in the metrics table. This funciton can calculate insert sizes when needed.
    """
    from glob import glob
    import pysam
    from numpy import median

    sample_dict = {}
    #print(str(glob( analysis_folder + "/alignments/*_1.bam")))

    for original_bam in glob( analysis_folder + "/alignments/*_1.bam"):
        print("Calculating insert size for: " + original_bam)
        sample = os.path.basename(original_bam)[0:12]

        with pysam.AlignmentFile(original_bam, "rb") as fp:
            tlens = []
            for read in fp.fetch(until_eof=True):
                if read.is_read2 and read.is_proper_pair:
                    tlens.append(abs(read.template_length))
            sample_dict[sample] = median(tlens)
        
    data = pd.DataFrame.from_dict(sample_dict, orient='index')         
    data.columns = ['insert_size_median'] 
    data.to_csv(analysis_folder + "/metrics/sample_median_insert_sizes.txt")

    return data
            

def make_comparison_plots(analysis_folder):
    twelve_week_data = pd.read_csv('/rnd/bgould/SDTS/All_metrics_compare/Big_sample_metrics_table.csv', index_col=68)
    twelve_week_noNTC = twelve_week_data[twelve_week_data.dbsnp_rate != "None"]

    flowcell_data = pd.read_csv(analysis_folder + "/metrics/all_metrics.table", sep="\t")
    flowcell_noNTC = flowcell_data[flowcell_data.dbsnp_rate != "None"]
    for word in analysis_folder.split("/"):
        if word.startswith("H"):
            flowcell_name = word[0:9]
            print("Processing flowcell: " + flowcell_name)
            break

	# MEAN COVERAGE
    plt.figure(1)
    sns.distplot(twelve_week_noNTC['mean_coverage'], hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label="Production Data")
    sns.distplot(flowcell_noNTC['mean_coverage'], hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label=str(flowcell_name))
    plt.legend(prop={'size':16})
    plt.title('MEAN COVERAGE')
    plt.xlabel('sample mean coverage depth')
    plt.ylabel('density')

    plt.savefig(analysis_folder + "/plots/" + flowcell_name + "_mean_coverage_compare.png") 

	# SD COVERAGE
    plt.figure(2)
    sns.distplot(twelve_week_noNTC['sd_coverage'], hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label="Production Data")
    sns.distplot(flowcell_noNTC['sd_coverage'], hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label=str(flowcell_name))
    plt.legend(prop={'size':16})
    plt.title('SD COVERAGE')
    plt.xlabel('stdev of depth of sample coverage')
    plt.ylabel('density')

    plt.savefig(analysis_folder + "/plots/" + flowcell_name + "_sd_coverage_compare.png")

    # GC BIAS SLOPE
    plt.figure(3)
    sns.distplot(twelve_week_noNTC['gc_bias_slope'], hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label="Production Data")
    sns.distplot(flowcell_noNTC['gc_bias_slope'], hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label=str(flowcell_name))
    plt.legend(prop={'size':16})
    plt.title('GC BIAS SLOPE')
    plt.xlabel('GC bias slope')
    plt.ylabel('density')

    plt.savefig(analysis_folder + "/plots/" + flowcell_name + "_gc_bias_slope_compare.png")

    # RELATIVE GC CONTENT   
    plt.figure(4)
    sns.distplot(twelve_week_noNTC['relative_gc_content'], hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label="Production Data")
    sns.distplot(flowcell_noNTC['relative_gc_content'], hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label=str(flowcell_name))
    plt.legend(prop={'size':16})
    plt.title('RELATIVE GC CONTENT')
    plt.xlabel('relative GC content')
    plt.ylabel('density')

    plt.savefig(analysis_folder + "/plots/" + flowcell_name + "_relative_gc_content_compare.png")

    # UNIQUE START SITES
    plt.figure(5)
    sns.distplot(twelve_week_noNTC['unique_start_sites'], hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label="Production Data")
    sns.distplot(flowcell_noNTC['unique_start_sites'], hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label=str(flowcell_name))
    plt.legend(prop={'size':16})
    plt.title('Unique start sites')
    plt.xlabel('no. unique start sites')
    plt.ylabel('density')

    plt.savefig(analysis_folder + "/plots/" + flowcell_name + "_unique_start_sites_compare.png")   

    # MAPPED READS
    plt.figure(6)
    sns.distplot(twelve_week_noNTC['mapped_reads'], hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label="Production Data")
    sns.distplot(flowcell_noNTC['mapped_reads'], hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label=str(flowcell_name))
    plt.legend(prop={'size':16})
    plt.title('MAPPED READS')
    plt.xlabel('mapped reads per sample')
    plt.ylabel('density')

    plt.savefig(analysis_folder + "/plots/" + flowcell_name + "_mapped_reads_compare.png")

    #FRACTION COVERED
    plt.figure(7)
    sns.distplot(twelve_week_noNTC['fraction_covered'], hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label="Production Data")
    sns.distplot(flowcell_noNTC['fraction_covered'], hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label=str(flowcell_name))
    median_fcov = float(median(flowcell_noNTC['fraction_covered']))
    plt.xlim((0.95,1.0))
    plt.legend(prop={'size':16})
    plt.title('MEDIAN FRACTION COVERED: ' + str("%.2f" % float(median_fcov)))
    plt.xlabel('fraction ROI covered (>=20X) per sample')
    plt.ylabel('density')
    
    plt.savefig(analysis_folder + "/plots/" + flowcell_name + "_fraction_covered_compare.png")

    # INSERT SIZE MEDIAN
    if 'insert_size_median' not in flowcell_noNTC.columns and not os.path.exists(analysis_folder + "/metrics/sample_median_insert_sizes.txt"):
        _data = calc_insert_sizes(analysis_folder)
        flowcell_data = _data['insert_size_median']
    elif os.path.exists(analysis_folder + "/metrics/sample_median_insert_sizes.txt"):
        flowcell_data = pd.read_csv(analysis_folder + "/metrics/sample_median_insert_sizes.txt")['insert_size_median']
    else:
        flowcell_data = flowcell_noNTC['insert_size_median']

    plt.figure(8)
    sns.distplot(twelve_week_noNTC['insert_size_median'], hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label="Production Data")
    sns.distplot(flowcell_data, hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label=str(flowcell_name))
    #plt.xlim((0.95,1.0))
    plt.legend(prop={'size':16})
    plt.title('MEDIAN INSERT SIZE')
    plt.xlabel('sample insert size median (bp)')
    plt.ylabel('density')

    plt.savefig(analysis_folder + "/plots/" + flowcell_name + "_insert_size_median_compare.png")
    
    # Contamination
    plt.figure(9)
    sns.distplot(twelve_week_noNTC['contamination_estimate'], hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label="Production Data")
    sns.distplot(flowcell_noNTC['contamination_estimate'], hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label=str(flowcell_name))
    plt.legend(prop={'size':16})
    plt.title('CONTAMINATION (percent)')
    plt.xlabel('contamination')
    plt.ylabel('density')

    plt.savefig(analysis_folder + "/plots/" + flowcell_name + "_contamination_estimate_compare.png")

    # dbSNP rate
    plt.figure(10)
    sns.distplot(twelve_week_noNTC['dbsnp_rate'].apply(pd.to_numeric, errors='ignore'), hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label="Production Data")
    sns.distplot(flowcell_noNTC['dbsnp_rate'].apply(pd.to_numeric, errors='ignore'), hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label=str(flowcell_name))
    plt.legend(prop={'size':16})
    plt.title('% sample snps that are in dbSNP')
    plt.xlabel('dbSNP RATE')
    plt.ylabel('density')

    plt.savefig(analysis_folder + "/plots/" + flowcell_name + "_dbsnp_rate_compare.png")

    # DEDUPED_READS
    plt.figure(11)
    sns.distplot(twelve_week_noNTC['deduped_reads'].apply(pd.to_numeric, errors='ignore'), hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label="Production Data")
    sns.distplot(flowcell_noNTC['deduped_reads'].apply(pd.to_numeric, errors='ignore'), hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label=str(flowcell_name))
    plt.legend(prop={'size':16})
    plt.title('DEDUPED READS')
    plt.xlabel('no. deduped reads per sample')
    plt.ylabel('density')
    
    plt.savefig(analysis_folder + "/plots/" + flowcell_name + "_deduped_reads_compare.png")

    # PCR_DUPE_METRIC
    plt.figure(12)
    sns.distplot(twelve_week_noNTC['pcr_dupe_metric'].apply(pd.to_numeric, errors='ignore'), hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label="Production Data")
    sns.distplot(flowcell_noNTC['pcr_dupe_metric'].apply(pd.to_numeric, errors='ignore'), hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label=str(flowcell_name))
    plt.legend(prop={'size':16})
    plt.title('DEDUPED READS')
    plt.xlabel('sample pcr dupe metric')
    plt.ylabel('density')

    plt.savefig(analysis_folder + "/plots/" + flowcell_name + "_pcr_dupe_metric_compare.png")

    print("Plots writen to " + flowcell_name + " /plots directory.")

    return 

if __name__=='__main__':
    fire.Fire(make_comparison_plots)
#    print "Plots writen to " + flowcell_name + " /plots directory."

