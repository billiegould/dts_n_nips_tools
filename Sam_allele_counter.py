# This script will create simulated read data containing deletions in the genome. Reads will be simultaneously downsampled to X coverage.

import sys
from pysam import AlignmentFile
from random import randrange


if len(sys.argv) < 6:
    sys.stderr.write("\n\n Useage: python deletion_data_generator.py deletions.bed nondeletions.bed infile.bam downsample_to_cov samplename > outfile.bam \n\n")
    sys.exit()

sample_coverages = { "s5517":34.0 , "s5518":37.7 , "s5520Pel":35.5 , "s5520Buffy":28.1 }

sample = str(sys.argv[5])

downsample_prob = float(sys.argv[4])/(sample_coverages[sample])

bamfile = AlignmentFile(sys.argv[3], "rb")

outfile = AlignmentFile("-", "w", template=bamfile)

deletions = open(sys.argv[1], "r")

## read sampling for deleted areas:
for line in deletions:
    chrom = line.split()[0]
    start = int(line.split()[1])
    end = int(line.split()[2])

    for read in bamfile.fetch(chrom, start, end):
        num = randrange(0,1)
        if num <= downsample_prob:
            num2 = randrange(0,1)
            if num2 <= 0.5:                 # downsample to a heterozygous deletion
                outfile.write(read)

## read sampling for non-deleted areas:
non_deletions = open(sys.argv[2], "r")

for line2 in non_deletions:
    chrom = line2.split()[0]
    start = int(line2.split()[1])
    end = int(line2.split()[2])

    for read in bamfile.fetch(chrom, start, end):
        num = randrange(0,1)
        if num <= downsample_prob:
            outfile.write(read)

outfile.close()
bamfile.close()
deletions.close()
non_deletions.close()

sys.stderr.write("Completed subsampling deletions. Coverage: " + str(sys.argv[3]) )
