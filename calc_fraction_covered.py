"""
This script will calculate the fraction covered across an entire custom roi.bed regions file for each passing sample
in a flowcell (>= 50x coverage) and a median value for each roi. Frac.covered =  %bases with >= 20X covereage. Sample statistics are output to file
with optional barplot. ROIs with <= 20x covereage in the lowest passing sample are also written to file. 
"""

import os
import pandas as pd
import matplotlib
matplotlib.use('agg')
from matplotlib import pyplot as plt
from collections import defaultdict
from numpy import median
import fire
import pandas as pd
from glob import glob

from seq_pipeline.utils.interval_list import IntervalList
from seq_pipeline.metrics.coverage_qc import compute_coverage



def get_passing_samples(analysis_dir):
    with open( analysis_dir + "metrics/all_metrics.table", "r") as data:
        metrics = pd.read_table(data, header=0)

    passing_samples = metrics.loc[metrics['mean_coverage']>= 50.0, ['identifier', 'mean_coverage']]

    lowest_passing_sample = str(passing_samples.loc[passing_samples['mean_coverage']==min(passing_samples.mean_coverage), 'identifier'].iloc[0])
    print "LOWEST PASSING SAMPLE: " + lowest_passing_sample
    
    return list(passing_samples['identifier']), lowest_passing_sample


def calc_sample_cov(sample, analysis_dir, interval_list):
    bam_file = (analysis_dir + "alignments/" + sample + "_1.final.bam")
    coverage = compute_coverage(bam_file, interval_list)

    return coverage


def frac_cov_stats(analysis_dir, roi_file):
    passing_samples, lowest_passing_sample = get_passing_samples(analysis_dir)

    # calculate total bases in the ROI
    interval_list_name = os.path.basename(roi_file).split(".")[0]
    interval_list = IntervalList(roi_file)
    
    bases_in_ROI = 0
    with open(roi_file, "r") as fp:
        for line in fp:
            start, end = line.split()[1:3]
            bases_in_ROI += (int(end) - int(start))
    print "Bases in ROI: " + str(bases_in_ROI)

    samples_frac_cov = {}
    roi_frac_cov = {}
    i = 0
    for sample in passing_samples:
        sample_dict = calc_sample_cov(sample, analysis_dir, interval_list)
        
    #calculate each sample total fraction covered
        bases_under_20x = 0
        for roi, cov_trace in sample_dict.iteritems():
            roi_len = len(cov_trace)
            roi_low_bases = len([base for base in cov_trace if base < 20])
            bases_under_20x += roi_low_bases
            
            if sample == lowest_passing_sample:
                roi_frac_cov[roi] = ( (roi_len - roi_low_bases)/float(roi_len) * 100 )
                #print roi, str(roi_low_bases), str(roi_len), str( (roi_len - roi_low_bases)/float(roi_len) * 100 )
                i += 1
        
        samples_frac_cov[sample] = (bases_in_ROI - bases_under_20x)/float(bases_in_ROI) * 100
        #print sample, str(bases_under_20x), str(bases_in_ROI)    
    
    print "reported rois : " + str(i)

    with open( analysis_dir + "metrics/" + interval_list_name + "_sample_frac_cov.csv", "w") as fp:
        for sample, cov in samples_frac_cov.iteritems():
            fp.write( str(sample) + "," + str(cov) + "\n")

    # write roi's with low fraction covered in the lowest passing sample to file
    with open( analysis_dir + "metrics/" + interval_list_name + "_low_frac_cov_rois.csv", "w") as fp:
        for roi, fc in roi_frac_cov.iteritems():
            if fc < 99.0:
                fp.write( roi + "," +  str(fc) + "\n")

    print "Coverage calculations complete."

    return


def plot_frac_cov(analysis_dir, roi_file):
    interval_list_name = os.path.basename(roi_file).split(".")[0]

    if not os.path.exists( analysis_dir + "metrics/" + interval_list_name + "_sample_frac_cov.csv" ):
        print "Stats file missing. Run fraction covered calculations first."
    else:
        with open( glob(analysis_dir + "metrics/" + "*_low_frac_cov_rois.csv")[0], "r") as fp:
            num_low_rois = len(fp.readlines())

        sample_data = pd.read_csv(analysis_dir + "metrics/" + interval_list_name + "_sample_frac_cov.csv", header=None)
        sample_data.columns = ["sample", "frac_cov"]
   
        plt.figure(1, figsize=(8,2))
        plt.title("sample " + interval_list_name + " fraction covered, (low rois, n=" + str(num_low_rois) +")")
        y_pos = range(sample_data.shape[0])
        plt.bar(y_pos, sample_data.frac_cov, color='g')
        plt.xticks(y_pos, [])
        plt.ylim(90,100)
        plt.xlabel("samples")
        plt.ylabel("fraction covered")
        plt.axhline(99, color='r', linestyle="--")
        plt.subplots_adjust(bottom=0.2, top=0.85)

        plt.savefig(analysis_dir + "plots/" + interval_list_name + "_sample_frac_cov.png")

        return

if __name__ =="__main__":
    fire.Fire()
