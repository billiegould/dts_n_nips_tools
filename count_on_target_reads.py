""" 
This script will write to file the number of off on-target reads for all passing samples (>= 50X mean cov) on a flowcell.
Stats are written to the metrics directory. Functions are written for reading production flowcells from OBS 
and for plotting the results against historical production data.
"""
import sys
import pysam
import fire
import pandas as pd
import stor

def get_passing_samples(open_metrics_file, lowest=False):
    print("Getting passing samples . . ")
    with open_metrics_file as fp:
        data = pd.read_table(fp)
        passing_samples = data.loc[ data['mean_coverage']>=50.0, ['identifier', 'mean_coverage']]

    if lowest == True:
        lowest_sample = passing_samples.loc[ passing_samples['mean_coverage'] == min(passing_samples['mean_coverage']), 'identifier']
        print("Lowest passing sample: " + str(lowest_sample))
        return str(lowest_sample.iloc[0])

    else:
        passing_samples = passing_samples['identifier']
        passing_samples = list(passing_samples.apply(lambda x: x.strip()))
        print("Passing samples: ", passing_samples)
        return passing_samples


def count_on_target_reads(bam_file):
    #print("counting " + str(bam_file) + " . . .")
    on_target_reads = 0
    total_reads = 0
    with pysam.AlignmentFile(bam_file) as reads:
        for read in reads:
            #print("len:" + str( read.infer_read_length()) + " unmapped:" + str(read.is_unmapped) + " proper_pair: " + str(read.is_proper_pair))
            if read.infer_read_length() <= 40 and not read.is_unmapped:
                if read.is_proper_pair and not read.mate_is_unmapped:
                    on_target_reads += 1
                total_reads += 1
        print("ontarget: " + str(on_target_reads) + " tot: " + str(total_reads))
        #sys.exit()
    
    return on_target_reads/float(total_reads)



def report_OT_reads(local_analysis_dir):

    open_metrics_file = open(local_analysis_dir + "metrics/all_metrics.table", "r")
    passing_samples = get_passing_samples(open_metrics_file)

    on_target_reads_dict = {}
    for sample in passing_samples:
        on_target_reads_dict[sample] = count_on_target_reads( local_analysis_dir + "alignments/" + sample + "_1.bam" )

    outfile = local_analysis_dir + "metrics/sample_on_target_reads.csv"
    with open(outfile, "w") as fp:
        for sample, percentage in on_target_reads_dict.items():
            fp.write(sample + "," + str(percentage) + "\n")

    print("Complete")


def report_OT_reads_stor(OBS_flowcell):

    open_metrics_file = stor.open("swift://AUTH_final_analysis_prod/" + OBS_flowcell + "analysis/metrics/all_metrics.table", "r")
    passing_samples = get_passing_samples(open_metrics_file)

    on_target_reads_dict = {}
    with stor.NamedTemporaryDirectory() as d:
        for sample in passing_samples:
            filename = "swift://AUTH_final_analysis_prod/" + OBS_flowcell + "analysis/alignments/" + sample + "_1.bam"
            dest = d + "/" + filename
            stor.Path(filename).copy(dest)
            
            on_target_reads_dict[sample] = count_on_target_reads(dest)

    outfile = "./" + str(OBS_flowcell[0:9]) + "_on_target_reads.csv"
    with open(outfile, "w") as fp:
        for sample, percentage in on_target_reads_dict.items():
            fp.write(sample + "," + str(percentage) + "\n")


def plot(local_analysis_dir):
    import os
    import matplotlib
    matplotlib.use('agg')
    from matplotlib import pyplot as plt
    import seaborn as sns  

    if os.path.exists(local_analysis_dir + "metrics/sample_on_target_reads.csv"):

        twelve_week_data = pd.read_csv('/rnd/bgould/SDTS/All_metrics_compare/12_fc_on_target_reads.csv', index_col=0)
        #twelve_week_data = twelve_week_data.drop('sample', axis=0) # remove header rows interspersed in the data file

        flowcell_data = pd.read_csv(local_analysis_dir + "metrics/sample_on_target_reads.csv", index_col=0)

        # a generic histogram plotter:
        plt.figure(1)
        sns.distplot(twelve_week_data.iloc[:,0].astype(float), hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label="Production Data (12 fcs)")
        sns.distplot(flowcell_data.iloc[:,0], hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3} )
        plt.legend(prop={'size':16})
        plt.title(str("PERCENT ON-TARGET READS"))
        plt.xlabel(str("sample % on-target reads"))
        plt.ylabel('density')

        plt.savefig(local_analysis_dir + "/plots/on_target_reads_compare.png")
        print("Complete.")
    else:
        print("Calculate om_target_reads before plotting.") 
    


if __name__=="__main__":
    fire.Fire()

