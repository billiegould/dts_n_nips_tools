# This script will identify sites with potential SNPs in a region of a bam containing pooled reads from multiple samples. SNPs are id'd according to a mapping quality of reads and a simple allele frequency threshold.
# The script cycles through all bases in the specified region of the alignment so will take a long time for large regions of the genome!

# Useage:   python Pooled_SNP_caller.py bamfile.bam chr1:1-1000 ref_genome.fa allele_Freq_threshold

import sys
from pysam import AlignmentFile, FastaFile
from re import split
from operator import itemgetter
from pandas import DataFrame


bamfile = AlignmentFile(sys.argv[1], "rb")

contig = sys.argv[2].split(":")[0]
start = int(split(":|-", sys.argv[2])[1])
stop = int(split(":|-", sys.argv[2])[2])

#print contig, start, stop

ref_genome = FastaFile(sys.argv[3])
convert_ref_mask = { "A":"A", "a":"A", "T":"T", "t":"T", "C":"C", "c":"C", "G":"G", "g":"G", "N":"N" }

af_thresh = float(sys.argv[4])

snps = []

print "Processing positions . . ."
for pileupcolumn in bamfile.pileup(contig=contig, start=start, stop=stop):

    _ref_base = ref_genome.fetch(reference=contig, start=pileupcolumn.reference_pos, end=(pileupcolumn.reference_pos +1))
    ref_base = convert_ref_mask[_ref_base]

    count_dict = dict(zip(["A", "T", "C", "G", "N", "indel"], [0, 0, 0, 0, 0, 0]))

    if pileupcolumn.reference_pos % 2000 == 0:
        print contig, pileupcolumn.reference_pos
        #from IPython import embed; embed()

    for pileupread in pileupcolumn.pileups:
        if pileupread.alignment.mapping_quality > 0:
            
            if pileupread.query_position != None:     # deletions in a read have query_position == none      

                read_base = pileupread.alignment.query_sequence[pileupread.query_position]
               # print read_base
                if read_base == "A":
                    count_dict["A"] += 1
                elif read_base == "T":
                    count_dict["T"] += 1
                elif read_base == "C":
                    count_dict["C"] += 1
                elif read_base == "G":
                    count_dict["G"] += 1
                elif read_base == "N":
                    count_dict["N"] += 1
                else:
                    count_dict["indel"] += 1    # count insertions
            else:
                count_dict["indel"] += 1        # count deletions

    total_uniques = float(sum([ count_dict[key] for key in count_dict.keys() ]))
    #print count_dict

    if total_uniques/pileupcolumn.nsegments > 0.98:                                       # if at least 98% of reads at a base are uniquely mapped
        #print total_uniques, pileupcolumn.nsegments
        ref_count = count_dict[ref_base]
        count_dict.pop(ref_base)
        #print max([count_dict[key]/total_uniques for key in count_dict.keys() ])

        if any([count_dict[key]/total_uniques > af_thresh for key in count_dict.keys() ] ):   # if there is a non-reference base at a site at a frequency above af_thresh
            alt_count = max(count_dict.iteritems(), key=itemgetter(1))[1]
            alt_base = max(count_dict.iteritems(), key=itemgetter(1))[0]
            count_dict.pop(alt_base)

            if max([count_dict[key]/total_uniques for key in count_dict.keys()]) < 0.05:       # and the next highest frequency allele base is at less than 5% frequency
                print [contig, pileupcolumn.reference_pos, ref_base, alt_base, ref_count/total_uniques, alt_count/total_uniques]
                snps.append( [contig, (pileupcolumn.reference_pos+1), ref_base, alt_base, ref_count/total_uniques, alt_count/total_uniques])  # record the snp location, add one to the position for compatibility with RH_genotyper.py 

    outfile = DataFrame( snps, columns=[ "chr", "pos", "REF", "ALT", "ref_freq", "alt_freq"])

    outfile.to_csv("markers.csv")
