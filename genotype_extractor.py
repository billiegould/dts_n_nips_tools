## This script will input a discovery VCF and extract allele counts, allele balance, and genotype calls for a single sample. Outputs st std out in tabular format.

## Currently skips INDELS. Later add on calling based on Haplotype Score flag

import sys
import fire
import vcf

base_index = {'A':0, 'C':1, 'G':2, 'T':3}
call_convert = {"0/0":0, "0/1":1, "1/1":2, "0|0":0, "0|1":1, "1|1":2, "./.":"no_call"}

if len(sys.argv) < 1:
    sys.stderr.write("Specify mode to run: homref OR discovery\n")
    sys.exit(1)


def tabulate_call_het(record):
    #print(record.REF)
    count_Ref = record.INFO['BaseCounts'][base_index[record.REF]]

    if len(record.ALT) >1:
        sys.stderr.write("WARNING:Multiple ALT alleles at base: "+ str(record.CHROM) + " "+ str(record.POS) + ". Using first one only.\n")

    count_Alt = record.INFO['BaseCounts'][base_index[str(record.ALT[0])]]

    try:
        allele_bal = record.INFO['ABHet']
    except KeyError:
        try:
            allele_bal = record.INFO['ABHom']
        except KeyError:
            allele_bal = "no_info"

    call = call_convert[record.samples[0]['GT']]

    results = [str(x) for x in [record.CHROM, record.POS, count_Ref, count_Alt, allele_bal, call]]

    return(results)


def tabulate_call_hom(record):
    #print(record.REF)
    count_Ref = record.samples[0]['AD'][0]

    if len(record.ALT) >1:
        sys.stderr.write("WARNING: Multiple ALT alleles at base: "+ str(record.CHROM) + " "+ str(record.POS) + ". Using first one only.\n")

    count_Alt = record.samples[0]['AD'][1]

    allele_bal = float(count_Ref)/(count_Ref + count_Alt)

    call = call_convert[record.samples[0]['GT']]

    results = [str(x) for x in [record.CHROM, record.POS, count_Ref, count_Alt, allele_bal, call]]
    return(results)




def discovery(input_vcf):
    ''' Processes *.discovery.vcf files'''

    sys.stdout.write("Chr\tPos\tcount_R\tcount_A\tallele_bal\tcall\n")
    vcf_reader = vcf.Reader(open(input_vcf, "r"))
    n = 0
    indels = 0
    for record in vcf_reader:
        n += 1
        try:
            record.INFO['IndelType']   #INDEL FLAG
            values = [str(record.CHROM), str(record.POS), "INDEL", "INDEL", "NA", "NA"]
            indels +=1
        except KeyError:
            values = tabulate_call_het(record)

        sys.stdout.write("\t".join(values) + "\n")

    sys.stderr.write("Skipped " + str(indels) + " indel genotoypes.\n")
    sys.stderr.write("Complete. Processed " +str(n)+ " sites\n")


def homref(input_vcf):
    ''' Processes *.homref_nondiscovery.vcf files'''

    sys.stdout.write("Chr\tPos\tcount_R\tcount_A\tallele_bal\tcall\n")
    vcf_reader = vcf.Reader(open(input_vcf, "r"))
    n = 0
    indels = 0
    for record in vcf_reader:
        n += 1
        if len(record.REF) >1 or any([len(x)>1 for x in record.ALT]):  #INDEL FLAG
            values = [str(record.CHROM), str(record.POS), "INDEL", "INDEL", "NA", "NA"]
            indels +=1
        else:
            values = tabulate_call_hom(record)

        sys.stdout.write("\t".join(values) + "\n")

    sys.stderr.write("Skipped " + str(indels) + " indel genotoypes.\n")
    sys.stderr.write("Complete. Processed " +str(n)+ " sites\n")


if __name__ == '__main__':
  fire.Fire()