# This script will make a simple plot of probe read counts by rank of the probe using the *probe_stats.probe_counts.csv file.


import numpy as np
import pandas as pd
import matplotlib
matplotlib.use('agg')
from matplotlib import pyplot as plt

import fire
from glob import glob

def plot_probe_counts(analysis_dir):

    with open(glob(analysis_dir +"/metrics/*.probe_stats.probe_read_counts.csv")[0], "r") as fp:
        counts = pd.read_csv(fp, index_col=0)

    _medians = np.median(counts+1, axis=1)
    medians = sorted(np.log(_medians))
    ranks = range(0,len(medians))
    
    plt.figure(1)
    plt.plot( ranks, medians, 'o')
    plt.ylabel("log probe read counts")
    plt.xlabel("Rank")
    plt.axhline(y=np.log(1000), linestyle="--", color="red")
    plt.axhline(y=np.log(5000), linestyle="--", color="red")
    plt.text(0,np.log(1000),"1000")
    plt.text(0,np.log(5000),"5000")

    plt.savefig(analysis_dir + "/plots/probe_counts.png") 

if __name__=="__main__":
    fire.Fire(plot_probe_counts)
