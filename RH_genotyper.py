import sys
import os
from pysam import AlignmentFile, FastaFile
import numpy as np
import pandas as pd
from itertools import product
from scipy import stats


CONTIG = "chr1"

def calc_snp_stats(bamfile, marker_df, reference=None):           
    snp_stats = []
    other_reads = 0
    for snp_num, row in marker_df.iterrows():
        #print row
        d_reads = 0 
        if reference:       # get the RHD reference base at the diffbase position
            convert_ref_mask = { "A":"A", "a":"A", "T":"T", "t":"T", "C":"C", "c":"C", "G":"G", "g":"G", "N":"N", "n":"N" }
            _ref_base = FastaFile(reference).fetch(reference = CONTIG, start= int(row[1]), end=int(row[2]))
            ref_base = convert_ref_mask[_ref_base]
            #print "position: ", str(row[1]), "ref base: ", str(ref_base)
        else:
            print "Specify reference genome file."
            sys.exit()

        for pileupcolumn in bamfile.pileup(contig = CONTIG, start= int(row[1]), stop=int(row[2]), truncate = True): 
            coverage = pileupcolumn.nsegments
            for pileupread in pileupcolumn.pileups:
                if pileupread.query_position != None:   # if the read base is a deletion, query position will be None
                    read_base = pileupread.alignment.query_sequence[pileupread.query_position]
                    if read_base == ref_base:
                        d_reads += 1
                    else:
                        other_reads += 1
                else:
                    other_reads += 1
        snp_stats.append(d_reads )
    #print pd.DataFrame(snp_stats)
    
    assert len(snp_stats) == marker_df.shape[0]
    
    # warn if there are non d/ce bases more than once per marker on average. snp coordinates are off or sequencing error is high. 
    if other_reads > marker_df.shape[0]:
        print "WARN: A lot of reads with non-reference diffbase: ", str(bamfile), str(other_reads), " out of ", str(sum(snp_stats, other_reads))     

    return snp_stats



def calc_sample_prob_matrix( snps, FF, avg_depth):

    logprob_data_matrix = pd.DataFrame(index=['f_0','f_1','f_2'], columns=['m_0','m_1','m_2'])
    
    E = 0.001 #this is the sequencing error, the chance of getting a D read in error

    for geno in product([0,1,2], repeat=2):           # returns: 00,01,02,10,11,12,20,21,22, for each genotype calculate probability of data as sum of sites
        #print geno
        f_geno = float(geno[0])
        m_geno = float(geno[1])
        tot_log_prob_data = None

        i=0
        for d_count in snps:    
            i+=1
            #print str(i), "geno, d_count, p: ", geno, d_count
    
            p = (0.5*((f_geno*FF) + (m_geno*(1-FF)) ))
            prob_data_given_geno = stats.poisson.pmf(k=d_count, mu=avg_depth*p)         ################ lynch pin           
            

            if tot_log_prob_data == None:
                tot_log_prob_data = np.log(prob_data_given_geno)
            else:
               # tot_prob_data_giv_geno *= prob_data_given_geno  #this statement is correct but it causes underflow
                tot_log_prob_data += np.log(prob_data_given_geno) 
       
               #print "snp probability: ", prob_data_given_geno                                       
               #print "geno cum. log probability: ", tot_log_prob_data

        logprob_data_matrix.ix[geno[0], geno[1]] = tot_log_prob_data 
    return logprob_data_matrix


if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument("--bam_list", required=False, dest="bam_list", help="list of bam files to process.")
    parser.add_argument("--marker_bed", required=False, dest="marker_bed", help="BED file with diff base positions")
    parser.add_argument("--workdir", required=False, dest="workdir", help="analysis directory containing alignments folder and metrics folder")
    parser.add_argument("--stats_file", default="infer_ff.basic_stats_human.csv", dest="FF_file", help="file containing fetal fraction info for each sample" )
    parser.add_argument("--ref_genome", required=False, dest="ref_genome", help="Optional. If specified, script will check and output the reference base at each marker snp.")    
    parser.add_argument("--out", required=True, dest="out_file", help="File name to write output data")
    parser.add_argument("--freq_file", required=False, dest="freq_file", help="File containing samples as rows and diff base allele frequencies as columns." \
                          "Columns are: index, sample_no, fetal_frac, total_reads, total_D_reads, mat_geno, fet_geno, db1_d_freq, db1_depth, db2_d_freq, db2_depth, . . .prop.RhD_difbases")
    parser.add_argument("--mode", required=True, dest="mode", help="Set to \"likelihood\" or \"bayes\", the later to use priors to call bayesian genotypes.")
    parser.add_argument("--depth", required=False, dest="avg_depth", help="the average sequencing depth (coverage) of the run")

    args = parser.parse_args()

    coverage_dict = {}
    FF_dict = {}

    if args.bam_list:
        #Input list of sample bam files
        with open(args.bam_list, "r") as fp:
            bams=[]
            for line in fp:
                bams.append(line)

        #Input list of snp markers and diff bases
        with open(args.marker_bed, "r") as fp:
            marker_df = pd.read_csv(fp, sep="\t")
        
        # get empirical depth data for each sample
        with open(args.workdir + "basic_stats/basic_stats_human.csv", "r") as fp:
            sample_depths = {}
            for line in fp:
                if "mapped_reads" not in line:
                    sample=line.split(",")[0]
                    mapped_reads = int(line.split(",")[3])
                    sample_depths[sample] = (mapped_reads * 35)/3157607875.0        # this denominator value for hg19.fa containing no adapters or other contigs
                    #print sample, "depth: ", sample_depths[sample] 

        #For each bam file or using the input simulated data file
        #1. Calculate read coverage for each allele at each site and add to snp data matrix
        df_snp_counts = pd.DataFrame(columns=map(str, marker_df.iloc[:,2]))
        #coverage_dict = {}
        for bam in bams:
            print "Genotyping BAM: ", bam
            bamfile = bam.strip()
            sample = os.path.basename(bamfile).split(".")[0]
            #name = '_'.join(sample.split("_")[1:4])
    
            with AlignmentFile(bamfile, "rb") as fp:
                df_snp_counts.loc[sample] = calc_snp_stats(fp, marker_df, reference=args.ref_genome)  #output one row with D read counts for each diff base
                coverage_dict[sample] = sum( df_snp_counts.loc[sample] )              
    
        #2. extract fetal fraction data from seqpipe files
        #FF_dict = {}
        with open(args.workdir+"infer_fetal_fraction/"+args.FF_file, "r") as fp:
            for line in fp:
                sample = line.split(',')[0]
                #name = '_'.join(sample.split("_")[2:5])
                FF_dict[sample] = line.split(',')[-2]
            #print FF_dict


    elif args.freq_file:
        sim_data = pd.read_csv(args.freq_file, delimiter=",", index_col=0)
        sim_data = sim_data.iloc[:, [1,2] + list( range(5,len(sim_data.columns)-1))]
        #print sim_data.iloc[0:1,:]

        df_snp_counts = pd.DataFrame(columns=map(str, sim_data.columns.values[2:]))
        for sample, row in sim_data.iterrows():        
            FF_dict[sample] = row[0]
            coverage_dict[sample] = row[1]
            sample_data = row[2:]
            df_snp_counts.loc[sample] = sample_data     
        
        sample_depths = dict(zip(list(df_snp_counts.index), np.repeat(args.avg_depth, df_snp_counts.shape[0])))




#3. For each sample, calculate probability of the observed read data for all genotypes given FF
    df_out = pd.DataFrame(columns= ["avg.depth", "d_reads", "sum.log.evidence_mat_D", "sum.log.evidence_fet_D", "mat_call", "fet_call", "log.call_posterior"])
    for name, row in df_snp_counts.iterrows():
        print "Processing sample: ", name
        sample_data_logprob_matrix = calc_sample_prob_matrix( row, float(FF_dict[name]), sample_depths[name])
        #print "LOG Prob. data: ", sample_data_logprob_matrix

        if args.mode == "bayes":
            # use Bayes theorem to calculate the posterior probability of each genotype in the sample matrix given the data
            # (posterior geno | fet_geno, mat_geno) = ((prob data | genos)*(prob genos))/(prob all observed data)

            prior_dict = {"00":0.0581, "01":0.0920, "02":0, "10":0.092 ,"11":0.238 , "12":0.148 , "20":0 , "21":0.146 ,"22":0.230 }

            posterior_geno_logprob_matrix = pd.DataFrame(index=['f_0','f_1','f_2'], columns=['m_0','m_1','m_2'])
        
            for index, n in np.ndenumerate(sample_data_logprob_matrix):
                if index != (2,0) and index != (0,2):
                    #logprob_data = sample_data_logprob_matrix.ix[ combo[0], combo[1]]
                    # note converstion: log(like) + log(prior) = log(likelihood*prior)
                    posterior_geno_logprob_matrix.ix[index[0], index[1]] = (n + np.log(prior_dict[''.join( [str(index[0]), str(index[1])] )]))
                else:
                    posterior_geno_logprob_matrix.ix[index[0], index[1]] = None  # for logprob_data=0, set log(logprob_data) <- None instead of -inf
        
                #print "Non-norm posterior LOG probs: " , posterior_geno_logprob_matrix

            # calculate the probability of all the data (denominator:
       
            # use the log sum exp trick:   
            # https://stats.stackexchange.com/questions/105602/example-of-how-the-log-sum-exp-trick-works-in-naive-bayes
            A = max([ i for index, i in np.ndenumerate(posterior_geno_logprob_matrix)])
            B = 0
            #log_denom = log(sum(exp(n)) = A + log(sum(exp(n-A))
            for index, n in np.ndenumerate(posterior_geno_logprob_matrix):
                if n != None:
                    B += np.exp(n-A)
                    #print "B", B
            log_denom = A + np.log(B) 
            #print "LOG denom:", log_denom

            # normalize the posteriors as a proportion of the probability of all the data (in log space)
            for index, n in np.ndenumerate(posterior_geno_logprob_matrix):
                if n != None:
                    posterior_geno_logprob_matrix.ix[index[0], index[1]] = posterior_geno_logprob_matrix.ix[index[0], index[1]] - log_denom

            print "Posterior LOG probs: ", posterior_geno_logprob_matrix
            print "Posterior probs sum: ", sum( [np.exp(n) for index, n in np.ndenumerate(posterior_geno_logprob_matrix) if n != None] )       

            d_reads = sum(row) 
            sum_logprob_mat_D = sum( (posterior_geno_logprob_matrix.ix[:,1:2]).sum(axis=1))
            sum_logprob_fet_D = sum( (posterior_geno_logprob_matrix.ix[1:2,:]).sum(axis=1))
       
            df = posterior_geno_logprob_matrix
            call = np.unravel_index( np.array(df).argmax(), np.array(df).shape)
            call_posterior = df.iloc[call[0],call[1]]
            print "call: ", call
            print "call log posterior:", call_posterior
            df_out.loc[name] = [sample_depths[name], d_reads, sum_logprob_mat_D, sum_logprob_fet_D, call[1], call[0], call_posterior]


        elif args.mode == "likelihood":
            d_reads = sum(row)
            df = sample_data_logprob_matrix
            
            print "Sample:" , name
            print "LOG Prob. data:"
            print df

            call = np.unravel_index( np.array(df).argmax(), np.array(df).shape)
            log_prob_call = max([ x for index, x in np.ndenumerate(df)])
            print "call: ", call
            df_out.loc[name] = [sample_depths[name], d_reads, log_prob_call, log_prob_call, call[1], call[0], "NA"]

    print df_out
    with open(args.out_file, "w") as fp:
        df_out.to_csv(fp)

#4. Plot the output


