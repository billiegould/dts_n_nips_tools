# This script will count coverage over all 56 idsnps and make a pie chart of all those with 
# <20X coverage in the lowest passing sample. It will output a list of all failing idsnp sites.


import pandas as pd
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import fire
import os

def main(wkdir, flowcell):
    
    # identify the lowest passing sample:
    with open(wkdir + "/metrics/all_metrics.table", "r") as fp:
        data = pd.read_table(fp)
        passing_samples = data.loc[ data['mean_coverage']>=50.0, ['identifier','mean_coverage']]
        lowest_sample = passing_samples.loc[ passing_samples['mean_coverage'] == min(passing_samples['mean_coverage']), 'identifier']

    # for the lowest passing sample count WL coverage 
    lowest_sample = str(lowest_sample.iloc[0])
    count_file = wkdir + "/alignments/" + lowest_sample + "_1.final.bam.counts"
    
    if not os.path.isfile(count_file):
        cmd = "bedtools coverage -counts -a /rnd/bgould/SDTS/balancing/Probe_selection/idsnp.roi.reportable.bed -b %salignments/%s_1.final.bam > %s" % (wkdir, lowest_sample, count_file)
        os.system(cmd)

    # read in count data and detect failing sites
    failing_sites = []
    with  open( count_file, "r") as fp:
        for line in fp:
            site = line.split()[2]
            cov = line.split()[9]
            if cov <= 20.0:
                failing_sites.append((site, cov))
    #plot
    plt.figure(1)
    plt.pie(x=[408-len(failing_sites), len(failing_sites)], labels=['PASS','FAIL'], colors=['green', 'red'], autopct='%1.1f%%', startangle=140)
    plt.savefig(wkdir + "/alignments/" + flowcell + "idsnp_pie.png")

    # write failed site data to file
    with open(wkdir + "/alignments/failing_idsnp_sites.list", "w") as outfile:
        for item in failing_sites:
            outfile.write(str(item) + "\n")
    
    print("Complete.")     
 
if __name__=="__main__":
    fire.Fire(main)
