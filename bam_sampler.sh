#! /bin/bash

# this script will use samtools downsampling function to downsample amnio bam files to desired coverage. regions in an input bed file will be downsampled as deletions to 0.5X further. 
# bams are generated for each region of the genome and then merged together at the end to create *_merged.sam
# set up to run as an array job

# USEAGE: bam_sampler.sh SV_regions.bed nonSV_regions.bed infile_full_sort.bam desired_coverage sample_file_prefix

BAMFILE=$3
SVs=$1
nonSVs=$2
coverage=$4
SAMPLE=$5

WKDIR=${SAMPLE}_${coverage}X

mkdir -p $WKDIR

if [ SAMPLE=="laneM_5517CULTURE1" ]
then
    ds=$(echo "scale=3 ; ${coverage}/34.0" |bc)
fi

if [ SAMPLE=="laneM_5518CULTURE1" ]
then
    ds=$(echo "scale=3 ; ${coverage}/37.7" |bc)
fi

if [ SAMPLE=="laneM_5520PELLET" ]
then
    ds=$(echo "scale=3 ; ${coverage}/35.5" |bc)
fi

if [ SAMPLE=="laneM_5520BUFFY" ]
then
    ds=$(echo "scale=3 ; ${coverage}/28.1" |bc)
fi

SV_ds_prob=$(echo "scale=3; ${ds}*0.5" |bc)              # downsample for deletions

echo $ds
echo $SV_ds_prob

i=0
while read -r line; do
    i=$((i+1))
    chrom=$(echo $line | cut -d " " -f1); #echo $chrom
    start=$(echo $line | cut -d " " -f2); #echo $start
    end=$(echo $line | cut -d " " -f3); #echo $end
    samtools view -s ${SV_ds_prob} ${BAMFILE} ${chrom}:${start}-${end} > ${WKDIR}/${SAMPLE}_${SGE_TASK_ID}_${i}_out.sam; 
done < $SVs

i=0
while read -r line2; do
    i=$((i+1))
    chrom=$(echo $line2 | cut -d " " -f1); #echo $chrom
    start=$(echo $line2 | cut -d " " -f2); #echo $start
    end=$(echo $line2 | cut -d " " -f3); #echo $end
    samtools view -s ${ds} ${BAMFILE} ${chrom}:${start}-${end} > ${WKDIR}/${SAMPLE}_${SGE_TASK_ID}_${i}_comp_out.sam;
done < $nonSVs

echo "Finished sampling"
samtools view -H $BAMFILE > ${SAMPLE}_header.txt
echo "Merging . . ."
cat ${SAMPLE}_header.txt ${WKDIR}/${SAMPLE}_${SGE_TASK_ID}_*out.sam > ${WKDIR}/${SAMPLE}_${SGE_TASK_ID}_merged.sam
#echo "Sorting . . ."
#samtools sort -@ 11  -o ${WKDIR}/${SAMPLE}_${SGE_TASK_ID}_${4}X_1kb_merge_sort.sam ${WKDIR}/${SAMPLE}_${SGE_TASK_ID}_merged.sam

rm ${WKDIR}/${SAMPLE}_${SGE_TASK_ID}_*out.sam
#rm ${WKDIR}/${SAMPLE}_${SGE_TASK_ID}_merged.sam

echo "Complete"
