#!/bin/bash

#out_dir = ('/rnd/bgould/SDTS/balancing/analysis3/reads')
#alignments_dir = '/rnd/bgould/SDTS/balancing/analysis3/alignments'
#final_bams = sorted(['%s/%s' % (alignments_dir, i) for i in os.listdir(alignments_dir) if i.endswith('_1.final.bam')])
#errout = '/home/mark_theilmann/out/'

file=$(ls ${SGE_TASK_ID}_*_sorted.bam)

SAMPLE=$(basename $file _sorted.bam | cut -d "_" -f2-4)

ALN_DIR=$1

PROBES="/rnd/bgould/SDTS/probes.bed"

echo "Using $PROBES"

python /rnd/bgould/SDTS/balancing/probe_rebalancing/utils/add_probe_mpos.py \
--final_bam ${ALN_DIR}/${SAMPLE}_1.final.bam \
--out ./${SGE_TASK_ID}_${SAMPLE}_prb_adj.bam \
--probe_bam $file \
--probes_bed $PROBES \
&& samtools index ./${SGE_TASK_ID}_${SAMPLE}_prb_adj.bam

#for bamname in final_bams:
#    sample = bamname.split('/')[-1].split('.')[0]
#    cmd = ('python /rnd/bgould/SDTS/balancing/probe_rebalancing/utils/add_probe_mpos.py '
#           '--final_bam {final_bam} --out {outbam} --probe_bam {probe_bam} '
#           '--probes_bed {probes_bed} && '
#           'samtools index {outbam}')
#    fcmd = cmd.format(final_bam=bamname, outbam='%s/%s.rebalfinal.bam' % (out_dir, sample),
#                      probe_bam='%s/%s_3.bam' % (out_dir, sample[:-2]),
#                      probes_bed='%s/../../sdts_probes.bed' % (out_dir))
    # cmd = 'samtools index {outbam}'
    # fcmd = cmd.format(outbam='%s/%s.rebalfinal.bam' % (out_dir, sample))
    
    # bsub_cmd = 'bsub -o {out} -e {out} \"{cmd}\"'
    # fbsub_cmd = bsub_cmd.format(out=errout, cmd=fcmd)
    # os.system(fbsub_cmd)
