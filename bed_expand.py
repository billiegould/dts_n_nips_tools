#!/usr/bin/python

# this script will expand each region in a bed file by the specified number of base pairs. output to stdout as new bed file. 

import sys

from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument("--bed_file", dest="bed_file", help="input bed file")
parser.add_argument("--left", dest = "left_expand", type = int, help="number of bases to add upstream of bed intervals")
parser.add_argument("--right", dest= "right_expand", type = int, help="number of bases to add downstream of bed intervals")
args=parser.parse_args()

with open(args.bed_file, "r") as bed:
    for line in bed:
        data = line.strip().split()
        if len(data) == 4:
            chrom, start, end, name = data
        elif len(data) > 4:    
            chrom, start, end, name = data[0:4]
        else:
            print "malformed bed file."
            sys.exit()
    
        if args.left_expand:
            new_start = int(start) - args.left_expand 
        if args.right_expand:
            new_end = int(end) + args.right_expand
        
        if len(data) == 4:
            sys.stdout.write("%s\t%s\t%s\t%s\n" % (chrom, new_start, new_end, name))
        elif len(data) > 4:
            sys.stdout.write("%s\t%s\t%s\t%s\t%s\n" % (chrom, new_start, new_end, name, str("\t".join(data[4:]))))
