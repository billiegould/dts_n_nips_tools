#!/usr/bin/env python

# This script will create simulated read data containing deletions in the genome. Reads will be simultaneously downsampled to X coverage.

import sys
from pysam import AlignmentFile
from random import uniform

'''
    Useage: downsampler.py infile_Nsorted.bam desired_coverage sample_name outfile.bam 
'''

sys.stderr.write( " Is your input sorted by read name? \n ")

if len(sys.argv) < 5:
    sys.stderr.write("\n\nUseage: python downsampler.py infile.bam downsample_to_cov samplename outfile.sam \n")
    sys.exit()

    
sample_coverages = { "5517CULTURE1":34.0 , "5518CULTURE1":37.7 , "5520PELLET":35.5 , "5520BUFFY":28.1 , "5520PLASMA":21.7 , "5520SUPE":38.5 }

sample = str(sys.argv[3])

downsample_prob = float(sys.argv[2])/(sample_coverages[sample])

bamfile = AlignmentFile(sys.argv[1], "rb")

outfile = AlignmentFile(sys.argv[4], "wh", template=bamfile)   #write header with bam output

prev_mate_pos = None
counter = 0
for read in bamfile:
    
    counter += 1
    if counter % 1000000 == 0:
        sys.stderr.write("Processed: " + str(counter) + " reads . . . \n")

    if read.pos == '':      # skip blank lines at the end of the file
        continue
        
    if read.pos == prev_mate_pos:        #if the read is the mate of the previous sampled one, keep it
        outfile.write(read)
        #print read
    else:
        num = float(uniform(0, 1))                   # subsample each read
        if num <= downsample_prob:
            outfile.write(read)
        #    print read
            prev_mate_pos = read.next_reference_start     # this will return a few erroneous reads that are actually pcr duplicates, but is best for efficiency
            
bamfile.close()
sys.stdout.close()
sys.stderr.write("Completed subsampling: " + str(sys.argv[1]) + " at " + str(sys.argv[2]) + "X \n")
