import sys


def generate_risk_score(markers, patient_genotypes, snp_risks):
    PRS_vals = {}
    for patient in patient_genotypes.keys():
        doseages = patient_genotypes[patient]
        factors = dict( [ (snp_risks[marker], doseages[marker]) for marker in markers] )
        PRS_vals[patient] = sum([ (log(OR) * dose) for OR, dose in factors.items()])

    return PRS_vals

# generate a proportional risk for each patient
def run_cox_model(PRS_vals, patient_covariates):




if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument("--patient_data", dest="patient_data", req=True, help="data file with one line per patient: patient_id/covariate1/covariate2/marker1_doseage/marker2_doseage")
    parser.add_argument("--snp_risks", dest="snp_risks", req=True, help="data file with one line per snp: snp_rsID/gwas_odds_ratio")

    snp_risks = {}
    with open(args.snp_risks, "r") as fp:
        for line in fp:
            snp_id = line.split()[0]
            OR = line.split()[1]
            snp_risks[snp_id] = OR

    with open(args.patient_data, "r") as fp:
        header = fp.readline().split()
        markers = [ marker for marker in header if "rs" in marker]
        covariates = [ cov for cov in header if "rs" not in cov]
        assert len(markers)+len(covariates) == len(header)

        patient_genotypes = {}
        patient_covariates = {}
        for line in fp:
            patient = line.split()[0]
            patient_genotypes[patient] = line.split()[(len(covariates)+1):]
            patient_covariates[patient] = line.split()[1:(len(covariates)+1)]

    PRS_vals = generate_risk_score(markers, patient_genotypes, snp_risks)

    Risk_vals = run_cox_model(PRS_vals, patient_covariates)

    write.something(Risk_vals)