#!/usr/bin/env python

# modified from script by Peter Graumann

import pandas as pd
import fire
import yaml
import tabulate
import stor
import os
import re
from numpy import inf
import glob

def make_fc_rst_report(template_yaml, workdir, docdir, flowcell_name, super_toc_file):
    append_to_toc_tree(super_toc_file, f'{flowcell_name}/{flowcell_name}')
    RstReport(template_yaml, workdir, docdir, flowcell_name, super_toc_file)


class RstReport(object):
    def __init__(self, template_yaml, workdir, docdir, flowcell_name, super_toc_file):
        self.template = yaml.load(open(template_yaml, 'r'))
        self.workdir = workdir
        self.flowcell_name = flowcell_name
        self.outdir = ifnotexistmkdir(f'{docdir}/{flowcell_name}')
        self.plotdir = ifnotexistmkdir(f'{self.outdir}/plots')

        function_lookup = {'h1': self.make_header1,
                           'h2': self.make_header2,
                           'h3': self.make_header3,
                           'text': self.make_text,
                           'fig': self.make_figure,
                           'fig2': self.make_side_by_side_figs,
                           'table': self.make_table,
                           'link': self.make_link,
                           'list': self.file_to_list}
        self.make_toc()

        for section, contents_list in self.template['SECTIONS'].items():
            text = ''
            for (f_key, content) in contents_list:
                text += function_lookup[f_key](content)

            with open(f'{self.outdir}/{section}.rst', 'w') as outfile:
                outfile.write(text)


    def make_text(self, text):
        return text + "\n\n\n"

    def make_header1(self, text):
        ul = ''.join(['=' for l in text])
        return f'{text}\n{ul}\n'

    def make_header2(self, text):
        ul = ''.join(['-' for l in text])
        return f'{text}\n{ul}\n'

    def make_header3(self, text):
        ul = ''.join(['^' for l in text])
        return f'{text}\n{ul}\n'

    def make_table(self, rel_path):
        this_table = f'{self.workdir}/{rel_path}'
        df = pd.read_csv(this_table, index_col=0)
        return tabulate.tabulate(df, headers=df.columns, tablefmt='rst') + "\n\n\n"

    def file_to_list(self, data):
        title, filepath = data
        text = title + "\n" + "\n.. hlist::\n   :columns: 3\n\n"
        with open(glob.glob(self.workdir + filepath)[0], "r") as fp:
            for line in fp:
                text += "   * " + line.strip() + "\n"
        text += "\n"
        return text

    def make_figure(self, data):
        rel_path, fig_name = data
        print(rel_path)
        img_path = glob.glob(self.workdir + "/" + rel_path)[0]
        #basename = os.path.basename(img_path)
        #dest_path = f'{self.plotdir}/{basename}'
        #stor.copy(img_path, dest_path)		

        text = self.make_header3(fig_name)
        text += f'.. figure:: /{img_path}\n'
        text += '\t:width: 1000px\n'
        text += '\t:align: center\n\n\n'
        return text

    def make_side_by_side_figs(self, data):
        _figA_filename, _figB_filename = data
        print(_figA_filename, _figB_filename)
        figA_filename = glob.glob(self.workdir + _figA_filename)[0]
        figB_filename = glob.glob(self.workdir + _figB_filename)[0]
        #img_path = f'{self.workdir}/plots'
        #basename = os.path.basename(img_path)
        
        text = f'.. image:: /{figA_filename}\n'
        text += '\t:width: 49%\n'
        text += f'.. image:: /{figB_filename}\n'
        text += '\t:width: 49%\n\n\n'
        return text
        
   # def make_bedlist(self, bedfile):
   #     print("List", glob.glob(self.workdir + bedfile)) 
   #     try:
   #         regions = pd.read_table(glob.glob(self.workdir + bedfile)[0], header=None)
   #     except pd.errors.EmptyDataError:
   #         return "No low coverage ROIs listed \n\n"

    #    text_list = []
    #    i=0
    #    for ROI in regions.iloc[:,3]:
    #        text_list.append(str(ROI))
    #    text = "Low cov. Genes (" + str(len(set(text_list))) + ")\n\n" + \
    #        ".. hlist::\n   :columns: 3\n\n" 
    #    for item in sorted(set(text_list)):
    #        text += "   * " + str(item) + "\n"
    #    text += "\n"
    #    return text

    def make_link(self, data):
        link_text = data[0]
        print("link: " + str(self.workdir) + str(data[1]))
        path_to_file = glob.glob(self.workdir + data[1])[0]
        figure_link = path_to_file.replace('/rnd/', 'http://oak1-an01/bgould/')
        text = f'`{link_text} <{figure_link}>`_\n\n\n'
        return text

    def make_toc(self):
        text = self.make_header1(self.flowcell_name)
        text += ".. toctree::\n\t:maxdepth: 2\n\n"
        for section in self.template['SECTIONS'].keys():
            text += f"\t{section}\n"
        with open(f'{self.outdir}/{self.flowcell_name}.rst', 'w') as outfile:
            outfile.write(text)


#def append_to_toc_tree(toc_path, new_section):
#    toc_file_lines = []
#    sectionliststart = inf
#    new_section = new_section
#    existing_sections = []
#    with open(toc_path, "r") as toc_file:
#        for i, line in enumerate(toc_file):
#            if "maxdepth" in line:
#                sectionliststart = i + 1
#            if i > sectionliststart:
#                existing_sections.append(line.strip())
#                if not re.match(r'\s+\w+', line) and new_section not in existing_sections:
#                    toc_file_lines.append("   " + new_section)
#                    sectionliststart = inf
#            toc_file_lines.append(line.rstrip())
#
#    with open(toc_path, "w") as toc_file:
#        toc_file.write("\n".join(toc_file_lines))
#        toc_file.write("\n")

def append_to_toc_tree(super_toc_path, new_toc_page):
    with open(super_toc_path, "r") as super_toc_file:
        toc_lines = []
        for line in super_toc_file:
            toc_lines.append(line)
        toc_lines = toc_lines[0:-1]
        if ("   " + str(new_toc_page + "\n")) not in toc_lines:
            toc_lines.append(("   " + str(new_toc_page + "\n")))
        #print("TOC lines", str(toc_lines))

    with open(super_toc_path, "w") as toc_file:
        toc_file.write(''.join(toc_lines))
        toc_file.write("\n")


def ifnotexistmkdir(directory):
    '''
    If directory doesn't exist make it

    Args:
        directory (str): path to directory to check

    Retruns:
        str: directory returned
    '''
    if not os.path.exists(directory):
        os.makedirs(directory)
    return directory


if __name__ == '__main__':
    fire.Fire(make_fc_rst_report)
