#!/usr/bin/env python

import os
import pysam
import pandas
import sys
import numpy
import cPickle
from datetime import time
from tqdm import tqdm
import gzip


def sampling_instructions(new_sample, samples, probe_locations, probe_bams, bam_pool_size, recipe, probe_read_depth):
 
    sys.stderr.write("Writing sampling instructions for sample %s" % new_sample)
    sample_instructions = []

    for probe, info in tqdm(probe_locations.items()):
        if probe in list(recipe.probe):
           # print info
            chrom, start, end, strand = info
            bam_pool = numpy.random.choice(probe_bams, bam_pool_size)
            sample_median_probe_depth = probe_read_depth[new_sample].loc[probe]
            conc = float(recipe.loc[recipe.probe == probe].conc)
            target_number_reads = conc * sample_median_probe_depth

            probe_read_names_to_match = []
            for bam in bam_pool:
                probe_read_names = []
                for read in bam.fetch(contig=chrom, start=int(start), end=int(end)):
                    probe_read_names.append(read.query_name)
                if len(probe_read_names) > 0:
                    probe_read_names_to_match.extend(numpy.random.choice( probe_read_names, int(target_number_reads/len(bam_pool)) ))
        else:
            continue
  
        # info = [chrom, start, end, strand]
        sample_instructions.append( [probe, info, bam_pool, target_number_reads, probe_read_names_to_match] )
    
    return sample_instructions



def sample_reads(new_sample, sample_instructions, genomic_bams, probe_locations, probe_reach):
    sys.stderr.write("Sampling reads for sample %s " % new_sample)
        
    header = probe_bams[0].header
    header['RG'][0]['SM'] = new_sample
    lane = new_sample.split('_')[1]
    pu = header['RG'][0]['PU'].split('.')[0]
    header['RG'][0]['PU'] = pu + '.' + lane
        
    with( pysam.AlignmentFile( new_sample + "_1_new.bam", mode="wb", header=header )) as new_bam:
        for probe_info in tqdm(sample_instructions):
            probe_bam_pool = probe_info[2]
 #           genomic_bam_pool = [ pysam.AlignmentFile( genomic_bam_dir + (os.path.basename(probe_bam.filename)[0:11] + "_1.final.bam")) for probe_bam in bam_pool]
            chrom, start, end, strand = probe_info[1]
            target_number_reads = probe_info[3]
            probe_read_names_to_match = set(probe_info[4])

            for bam in genomic_bams:
                if (bam.filename)[0:11] in [ (probe_bam.filename)[0:11] for probe_bam in probe_bam_pool]:
                    #open_genomic_bam = pysam.AlignmentFile( bam, "rb")
                    if strand == "+":
                        read_pool = [ read for read in  bam.fetch(chrom, (int(end) + probe_reach)) ]
                    elif strand == "-":
                        read_pool = [ read for read in  bam.fetch(chrom, (int(start) - probe_reach))]

                    for read in read_pool:
                        if read.query_name in probe_read_names_to_match:
                            new_bam.write(read)
    return
                           

if __name__ == "__main__":

    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument("--info", help="This script will simulate a single sample on a balanced flowcell using an input balancing recipe. To simulate a whole flowcell, run this script in parallel on the batch queue.")
    parser.add_argument("--new_sample", dest="new_sample", help="the name of the new sample to simulate (e.g. s_1_AATGCAA), should match a sample name in the genomic_bams_list")
    parser.add_argument("--probe_bams_list", dest="probe_bams_list", help="list of full paths to probe read bam files (*_3.bam)", default=None)
    parser.add_argument("--genomic_bams_list", dest="genomic_bams_list", help="list of full paths to genomic read bams (*_1.final.bam). Do not list NTCs or non-passing samples", default=None)
    parser.add_argument("--probe_bed", dest="probe_bed", help="path to the file probes.bed")
    parser.add_argument("--recipe", dest="recipe", help="path to the balancing recipe file (df_probe2conc.csv). must have \"probe\" and \"conc\" columns")
    parser.add_argument("--probe_read_depth", dest="probe_read_depth", help="path to the merged probe read depth file generated by merge_counts.py", default=None)
    parser.add_argument("--bam_pool_size", dest="bam_pool_size", help="number of randomly chosen genomic bam files to sample to simulate capture reads for each probe, higher numbers will increase compute time", default=3)
    parser.add_argument("--probe_reach", dest="probe_reach", help="how far downstream (in bp) a probe is able to capture. run utils/histo.py to see this distance", default=700)
#    parser.add_argument("--output_prefix", dest="output_prefix", help="output prefix")
#    parser.add_argument("--concentration_multiplier", dest="concentration_multiplier", default=1.0, type=float)
#    parser.add_argument("--no_balancing", dest="no_balancing", default=False, action="store_true")

    args = parser.parse_args()

    # Load probe locations into a dictionary:
    probe_locations = {}
    with open(args.probe_bed, "r") as fp:
        for line in fp:
            chrom, start, end, probe, dot, strand = line.strip().split()[0:6]
            probe_locations[probe] = (chrom, int(start), int(end), strand)

    # Load probe bam file names into a list:
    with open(args.probe_bams_list, "r") as fp:
        probe_bam_file_names = [line.strip() for line in fp]
    probe_bams = [ pysam.AlignmentFile(bam) for bam in probe_bam_file_names]
    
    samples = [ os.path.basename(fname)[0:11] for fname in probe_bam_file_names ]

    #Load the probe balancing worklist into a pandas df (df_probe2conc.csv)
    recipe = pandas.read_csv(args.recipe)

    #load probe read depth file into a pandas df (probe_counts.csv)
    probe_read_depth = pandas.read_csv(args.probe_read_depth, index_col=0)

    #check that the sample to be simulated is tabulated in the probes read depth file:
    print probe_read_depth.columns.tolist()
    assert args.new_sample in probe_read_depth.columns.tolist()

#    from IPython import embed; embed()

    sampling_instructions = sampling_instructions(args.new_sample, samples, probe_locations, probe_bams, args.bam_pool_size, recipe, probe_read_depth)
    
    with open(args.genomic_bams_list, "r") as fp:
        genomic_bam_names = [line.strip() for line in fp]
    genomic_bams = [pysam.AlignmentFile(bam, "rb") for bam in genomic_bam_names]
    
    sample_reads( args.new_sample, sampling_instructions, genomic_bams, probe_locations, args.probe_reach)

    print "Complete"

# in-script multiprocessing has not been possible. multiple threads used by python multiprocessing cannot seem to access the same open bam files for sampling. parellize using the batch queue. 
