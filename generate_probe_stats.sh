#!/bin/bash

R_DIR="/rnd/bgould/SDTS/balancing/probe_rebalancing"  #git repo location for probe balancing software

D_DIR="."   # data directory where your probe adjusted bam files live

PROBES="/rnd/bgould/SDTS/probes.bed"   #path to your probes.bed file

BAM=$(ls ${SGE_TASK_ID}_*_prb_adj.bam)


echo "Using $PROBES"
echo $BAM

python ${R_DIR}/utils/probe_characterize.py \
    --bam_path $BAM \
    --probes_bed_path $PROBES \
    --out_dir $D_DIR            # write to the directory where the original bams are located


#old python code
#for bam in rebal_bams:
#    cmd = 'python {script} --bam_path {bam} --probes_bed_path {probes_bed} --out_dir {out_dir}'
#    fcmd = cmd.format(script='%s/utils/probe_characterize.py' % pooltest_dir, bam=bam, probes_bed=probes_bed, out_dir=out_dir)
#    bsub_cmd = 'bsub -o {out} -e {out} \"{cmd}\"'
#    fbsub_cmd = bsub_cmd.format(out=errout, cmd=fcmd)
#    os.system(fbsub_cmd)
