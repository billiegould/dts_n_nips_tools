# This script will calculate the A-Allele frequency for each snp in a .gen file 

import sys
import pandas as pd

sys.stderr.write("reading in gen file")
genfile = pd.read_csv(sys.argv[1], sep=" ")


# for each snp row, starting wth col 6, read in 3 numbers, choose highest, add allele count to total, repeat, 

n_indiv = ((genfile.shape[1])-5) / 3.0 
assert ((genfile.shape[1])-5) % 3 == 0

sys.stderr.write("counting alleles . . .")
for index, row in genfile.iterrows():
	snp_name = row.iloc[1]
	A_alleles = 0
	n = 2
	for i in range(int(n_indiv)):
		n += 3
		AA, AB, BB = row.iloc[n:n+3]
		if max(AA, AB, BB) == AA:
			A_alleles += 2
		elif max(AA, AB, BB) == AB:
			A_alleles += 1
		elif max(AA, AB, BB) == BB:
			A_alleles += 0
	
	A_AF = A_alleles / (2.0 * n_indiv)
	sys.stdout.write(str(snp_name) + "\t" + str(A_AF) + "\n")

sys.stderr.write("Complete")
