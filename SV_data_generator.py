## This script will generate simulated SVs from input bed files using read-name sorted bam files. Output will preserve paired reads.


import sys
from pysam import AlignmentFile
from random import uniform

sys.stderr.write("Remember: Input reads must be Name Sorted!")

if len(sys.argv) < 6:
    sys.stderr.write("\n\nUseage: python SV_data_generator.py SVs.bed SVs_comp.bed infile.bam downsample_to_cov samplename > outfile.bam \n")
    sys.exit()

sample_coverages = { "laneM_5517CULTURE1":34.0 , "laneM_5518CULTURE1":37.7 , "laneM_5520PELLET":35.5 , "laneM_5520BUFFY":28.1 }

sample = str(sys.argv[4])

downsample_prob = float(sys.argv[3])/(sample_coverages[sample])

bamfile = AlignmentFile(sys.argv[2], "rb")

outfile = AlignmentFile("-", "w", template=bamfile)  # this will output a sam file on stdout



SVs = open(sys.argv[1], "r")
prev_readname = None
keep = False
for line in SVs:
    chrom = line.split()[0]  # define the next deletion interval
    start = int(line.split()[1])
    end = int(line.split()[2])

    for read in bamfile.fetch(chrom, start, end):
        readname = read.AlignedSegment.query_name

        if readname == prev_readname and keep == True:    # if the first of the pair has been written, write the reverse read
            outfile.write(read)
            continue
        else:
            keep = False
            num = float(uniform(0, 1))           # subsample read for coverage
            if num <= downsample_prob:
                num2 = float(uniform(0, 1))
                if num2 <= 0.5:                 # downsample to a heterozygous deletion
                    outfile.write(read)
                    keep = True


nonSVs = open(sys.argv[2], "r")
prev_readname = None
keep = False
for line in nonSVs:
    chrom = line.split()[0]
    start = int(line.split()[1])
    end = int(line.split()[2])

    for read in bamfile.fetch(chrom, start, end):
        readname = read.AlignedSegment.query_name

        if readname == prev_readname and keep == True:  # if the first of the pair has been written, write the reverse read
            outfile.write(read)
            continue
        else:
            keep = False
            num = float(uniform(0, 1))  # subsample read for coverage
            if num <= downsample_prob:
                    outfile.write(read)
                    keep = True
outfile.close()
bamfile.close()
SVs.close()

sys.stderr.write("Completed subsampling deletions. Coverage: " + str(sys.argv[3]) )
