#!/bin/bash

python make_bam_to_test_recipe96_mrt.py --probe_bed ../sdts_probes_in_reportable2.bed --genomic_bam_list genomic_bams.list --probe_bam_list probe_bams.list --worklist ../df_probe2conc.csv --probe_read_depth ../probe_counts.csv --i $SGE_TASK_ID
