"""
This script will input the all_metrics.table file from a processed flowcell and 
output a bar chart of sample coverages (for bams present in the alignments directory).
The sample.list file should already be present in the alignments directory. 
"""

import matplotlib
matplotlib.use('agg')
from matplotlib import pyplot as plt

import pandas as pd
import fire
import operator
from numpy import arange

def plot_sample_cov(analysis_dir):
    
    samples_to_include = []
    with open( analysis_dir + "alignments/sample.list", "r") as sample_list:
        for line in sample_list:
            samples_to_include.append(line.strip())

    with open( analysis_dir + "metrics/all_metrics.table", "r") as data:
        metrics = pd.read_table(data, header=0)

    metrics_subset = metrics.loc[ metrics['identifier'].isin(samples_to_include), ['mean_coverage', 'fraction_covered']]
    metrics_sorted = metrics_subset.sort_values(by=['mean_coverage'])

    # plot mean coverage
    plt.figure(1, figsize=(8,2))
    plt.title("sample mean coverage")
    y_pos = arange(metrics_sorted.shape[0])
    plt.bar(y_pos, metrics_sorted.mean_coverage, color='b')
    #plt.xticks(y_pos, zip(*sorted_samples)[0], rotation=45)
    #plt.rc('axes', labelsize=9)
    plt.xticks(y_pos, [])
    plt.xlabel("samples")
    plt.ylabel("avg.read depth")
    plt.axhline(50, color='r', linestyle="--")
    plt.subplots_adjust(bottom=0.2) 

    plt.savefig(analysis_dir + "plots/sample_mean_coverage.png")

    # plot fraction covered
    plt.figure(2, figsize=(8,2))
    plt.title("sample fraction covered")
    y_pos = arange(metrics_sorted.shape[0])
    plt.bar(y_pos, metrics_sorted.fraction_covered, color='r')
    #plt.xticks(y_pos, zip(*sorted_samples)[0], rotation=45)
    #plt.rc('axes', labelsize=9)
    plt.xticks(y_pos, [])
    plt.ylim(0.98,1.0)
    plt.xlabel("samples")
    plt.ylabel("fraction covered")
    plt.axhline(0.997, color='r', linestyle='--')
    plt.subplots_adjust(bottom=0.2)
   
    plt.savefig(analysis_dir + "plots/sample_fraction_covered.png")

    print("Sample coverage plots complete")

if __name__=="__main__":
    fire.Fire(plot_sample_cov)
