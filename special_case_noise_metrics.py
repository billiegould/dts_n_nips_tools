"""
This script will calculate noise metrics for each special case caller. Noise metrics are output into a csv indexed by sample.
Noise metrics are generally the squared deviation from the neareast integer copy number call for normalized copy number calculations for each locus.
"""

import pandas as pd
import numpy as np
import fire
from glob import glob
import sys
import stor
import os
import matplotlib
matplotlib.use('agg')
from matplotlib import pyplot as plt
import seaborn as sns
from random import randint

# Get the list of passing samples (by mean coverage > 50X)

def get_passing_samples(open_metrics_file, lowest=False):
    print("Getting passing samples . . ")
    with open_metrics_file as fp:
        data = pd.read_table(fp)
        passing_samples = data.loc[ data['mean_coverage']>=50.0, ['identifier', 'mean_coverage']]
        
    if lowest == True:
        lowest_sample = passing_samples.loc[ passing_samples['mean_coverage'] == min(passing_samples['mean_coverage']), 'identifier']
        print("Lowest passing sample: " + str(lowest_sample))
        return str(lowest_sample.iloc[0])

    else:
        passing_samples = passing_samples['identifier']
        passing_samples = list(passing_samples.apply(lambda x: x.strip()))
        print("Passing samples: ", passing_samples)
        return passing_samples

def calculate_AT_noise(df_final_1, df_final_2, passing_samples):
    print("Calculating AT noise . . .")
    #df_final_1 = pd.read_csv( glob(wkdir + "/metrics/df_AT_final_*_lane1.csv")[0], header=0)
    #df_final_2 = pd.read_csv( glob(wkdir + "/metrics/df_AT_final_*_lane2.csv")[0], header=0)
    df_final = pd.concat([df_final_1, df_final_2])
    df_final = df_final.set_index('identifier')
    df_final = df_final.drop(['sample_id','barcode', 'Unnamed: 0', 'valid_gc_bias'], axis=1)
    cols = [col for col in df_final.columns if 'degen' not in col] #exclude degenerate probes from teh noise calculation 
    #print(df_final.iloc[0:5,0:5])

    avg_AT_noise = {}
    avg_ltc_noise = {}
    for sample in passing_samples:
        AT_sample_noise = 0
        ltc_sample_noise = 0
        num_AT_probes = 0
        num_ltc_probes = 0
        for probe in cols:
            if "AT" in probe:
                CN = float(df_final.loc[sample, probe]) * 2
                CN_dev = (CN - np.rint(CN))**2
                AT_sample_noise += CN_dev
                num_AT_probes += 1
            else:
                CN = float(df_final.loc[sample, probe]) * 2
                CN_dev = (CN - np.rint(CN))**2
                ltc_sample_noise += CN_dev
                num_ltc_probes += 1
        avg_AT_noise[sample] = AT_sample_noise/num_AT_probes
        avg_ltc_noise[sample] = ltc_sample_noise/num_ltc_probes   

    return avg_AT_noise, avg_ltc_noise
        

def get_CAH_noise(df_CAH, passing_samples):
    print("Getting CAH noise . . .")
    samples_CAH_noise_dev = {}
    samples_CAH_noise_sum = {}
    for sample in passing_samples:
        samples_CAH_noise_dev[sample] = float(df_CAH.loc[df_CAH['identifier'] == sample, 'CAH_cn_dev'])
        samples_CAH_noise_sum[sample] = float(df_CAH.loc[df_CAH['identifier'] == sample, 'CAH_cn_sum'])

    #print(samples_CAH_noise_dev.items()[0:3])
    
    return samples_CAH_noise_dev, samples_CAH_noise_sum


def calculate_SMN_noise(df_SMN_1, df_SMN_2, passing_samples):
    print("Calculating SMN noise . . .")
    #df_SMN_1 = pd.read_csv( wkdir + "/metrics/df_sma_final_postfit_lane1.csv")
    #df_SMN_2 = pd.read_csv( wkdir + "/metrics/df_sma_final_postfit_lane2.csv")         
    df_SMN_final = pd.concat( [df_SMN_1, df_SMN_2])
    df_SMN_final = df_SMN_final.iloc[:,:-6]  #remove the last six columns which are final cn calls and lods
    df_SMN_final = df_SMN_final.rename(columns = {"Unnamed: 0":"barcode"})  #first column has no label and contains only bases in the barcode
    df_SMN_final = df_SMN_final.set_index('barcode')
    
    #prefixes = list("s_1_"*48) + list("s_2_"*48)
    labels1 = [("s_1_" + label) for label in df_SMN_final.index[0:48]]
    labels2 = [("s_2_" + label) for label in df_SMN_final.index[48:96]]
    df_SMN_final.index = labels1 + labels2   
    print(df_SMN_final)
 
    avg_sample_SMN_noise ={}
    avg_sample_control_noise = {}
    for sample in passing_samples:
        #sample = s[4:]          #reduce the sample name to the barcode only 
        CN_SMN_sum=0
        CN_control_sum=0
        SMN_probes = []
        control_probes = []
        for probe in df_SMN_final.columns:
            if 'SMA' in probe or 'sma' in probe or 'SMN' in probe:
                SMN_probes.append(probe)
                #print(df_SMN_final.loc[sample, probe])
                CN_SMN = float(df_SMN_final.loc[sample, probe])
                CN_SMN_dev = (CN_SMN - np.rint(CN_SMN))**2
                CN_SMN_sum += CN_SMN_dev
            else:
                control_probes.append(probe)
                CN_control = float(df_SMN_final.loc[sample, probe])
                CN_control_dev = (CN_control - np.rint(CN_control))**2
                CN_control_sum += CN_control_dev
        
        avg_sample_SMN_noise[sample] = CN_SMN_sum/len(set(SMN_probes))
        avg_sample_control_noise[sample] = CN_control_sum/len(set(control_probes))

    return avg_sample_SMN_noise, avg_sample_control_noise



def calculate_GBA_noise(df_GBA_1, df_GBA_2, passing_samples):
    print("Calculating GBA noise . . .")
    #df_GBA_1 = pd.read_csv( wkdir + "/metrics/df_ltc_GBA_lane_1_final.csv")
    #df_GBA_2 = pd.read_csv( wkdir + "/metrics/df_ltc_GBA_lane_2_final.csv") 
    df_GBA_final = pd.concat( [df_GBA_1, df_GBA_2])
    df_GBA_final = df_GBA_final.iloc[:,:-2]  #remove the last two columns which are sample id and well
    df_GBA_final = df_GBA_final.drop(['Unnamed: 0'], axis=1)
    df_GBA_final = df_GBA_final.set_index('barcode')
    #print(df_GBA_final.iloc[0:5,0:5])

    avg_sample_GBA_noise ={}
    avg_sample_control_noise = {}
    for sample in passing_samples:
        CN_GBA_sum=0
        CN_control_sum=0
        for probe in df_GBA_final.columns:
            if 'GBA' in probe:
                CN_GBA = float(df_GBA_final.loc[sample, probe])
                CN_GBA_dev = (CN_GBA - np.rint(CN_GBA))**2
                CN_GBA_sum += CN_GBA_dev
            else:
                CN_control = float(df_GBA_final.loc[sample, probe])
                CN_control_dev = (CN_control - np.rint(CN_control))**2
                CN_control_sum += CN_control_dev

        avg_sample_GBA_noise[sample] = CN_GBA_sum/len([probe for probe in df_GBA_final.columns if 'GBA' in probe])
        avg_sample_control_noise[sample] = CN_control_sum/len([probe for probe in df_GBA_final.columns if 'GBA' not in probe])

    return avg_sample_GBA_noise, avg_sample_control_noise


def calc_noise_metrics(wkdir):

    passing_samples = get_passing_samples( open(wkdir + "/metrics/all_metrics.table", "r"))

    # AT
    df_final_1 = pd.read_csv( glob(wkdir + "/metrics/df_AT_final_*_lane1.csv")[0], header=0)
    df_final_2 = pd.read_csv( glob(wkdir + "/metrics/df_AT_final_*_lane2.csv")[0], header=0)
    samples_AT_noise, samples_ATctrl_noise = calculate_AT_noise(df_final_1, df_final_2, passing_samples) # returns dict
    
    # CAH    
    with open( wkdir + "/metrics/all_metrics.table", "r") as data:
        df_CAH = pd.read_table(data)
    samples_CAH_noise_dev, samples_CAH_noise_sum  = get_CAH_noise(df_CAH, passing_samples) # two dict
        
    # GBA
    df_GBA_1 = pd.read_csv( wkdir + "/metrics/df_ltc_GBA_lane_1_final.csv")
    df_GBA_2 = pd.read_csv( wkdir + "/metrics/df_ltc_GBA_lane_2_final.csv")   
    samples_GBA_noise, samples_GBAcontrol_noise = calculate_GBA_noise(df_GBA_1, df_GBA_2, passing_samples) #two dict        
    
    # SMN
    df_SMN_1 = pd.read_csv( wkdir + "/metrics/df_sma_final_postfit_lane1.csv")
    df_SMN_2 = pd.read_csv( wkdir + "/metrics/df_sma_final_postfit_lane2.csv")
    samples_SMN_noise, samples_SMNcontrol_noise = calculate_SMN_noise(df_SMN_1, df_SMN_2, passing_samples)        

    #write
    with open( wkdir + "metrics/df_special_case_noise.csv", "w") as outfile:
        merge_and_write(outfile, samples_AT_noise, samples_ATctrl_noise, samples_CAH_noise_dev, samples_CAH_noise_sum, samples_GBA_noise, samples_GBAcontrol_noise, samples_SMN_noise, samples_SMNcontrol_noise)

    print("Complete. Stats written to metrics/df_special_case_noise.csv")
    
    return



def calc_noise_metrics_stor(stor_flowcell):
    
    stor_path = find_run_folder(stor_flowcell)
    
    passing_samples = get_passing_samples( stor.Path(stor_path + "metrics/all_metrics.table").open("r"))

    # AT
    df_final_1 = pd.read_csv( stor.Path( stor_path + "metrics/df_AT_final_" + stor_flowcell + "_lane1.csv").open("r"), header=0)
    df_final_2 = pd.read_csv( stor.Path( stor_path + "metrics/df_AT_final_" + stor_flowcell + "_lane2.csv").open("r"), header=0)
    samples_AT_noise, samples_ATctrl_noise = calculate_AT_noise(df_final_1, df_final_2, passing_samples) # returns dict

    # CAH
    df_CAH = pd.read_table( stor.Path( stor_path + "metrics/all_metrics.table").open("r"))
    samples_CAH_noise_dev, samples_CAH_noise_sum  = get_CAH_noise(df_CAH, passing_samples) # two dict

    # GBA
    df_GBA_1 = pd.read_csv( stor.Path( stor_path + "metrics/df_ltc_GBA_lane_1_final.csv").open("r"))
    df_GBA_2 = pd.read_csv( stor.Path( stor_path + "metrics/df_ltc_GBA_lane_2_final.csv").open("r"))
    samples_GBA_noise, samples_GBAcontrol_noise = calculate_GBA_noise(df_GBA_1, df_GBA_2, passing_samples) #two dict

    # SMN
    df_SMN_1 = pd.read_csv( stor.Path( stor_path + "metrics/df_sma_final_postfit_lane1.csv").open("r"))
    df_SMN_2 = pd.read_csv( stor.Path( stor_path + "metrics/df_sma_final_postfit_lane2.csv").open("r"))
    samples_SMN_noise, samples_SMNcontrol_noise = calculate_SMN_noise(df_SMN_1, df_SMN_2, passing_samples)

    #write
    with open( stor_flowcell + "_special_case_noise.csv", "w") as outfile:
        merge_and_write(outfile, samples_AT_noise, samples_ATctrl_noise, samples_CAH_noise_dev, samples_CAH_noise_sum, samples_GBA_noise, samples_GBAcontrol_noise, samples_SMN_noise, samples_SMNcontrol_noise)

    print("Complete. Stats written to current directory")



def find_run_folder(stor_flowcell):
    path_list = [i for i in stor.Path('swift://AUTH_final_analysis_prod/').listdir() if stor_flowcell in i]
    print(path_list)
    try:
        len(path_list) == 1
    except AssertionError:
        print("Flowcell not found or more than one flowcell found. Skipping.")
        sys.exit()

    path = path_list[0] + "/analysis/"

    return path


def merge_and_write(outfile, samples_AT_noise, samples_ATctrl_noise, samples_CAH_noise_dev, samples_CAH_noise_sum, samples_GBA_noise, samples_GBAcontrol_noise, samples_SMN_noise, samples_SMNcontrol_noise):
    #merge the data together
    names = ['sample', 'AT_noise', 'AT_ctrl_noise', 'CAH_dev', 'CAH_sum', 'GBA_noise', 'GBA_ctrl_noise', 'SMN_noise', 'SMN_ctrl_noise']
    ds = [samples_AT_noise, samples_ATctrl_noise, samples_CAH_noise_dev, samples_CAH_noise_sum, samples_GBA_noise, samples_GBAcontrol_noise, samples_SMN_noise, samples_SMNcontrol_noise]
    out_data = {}
    for sample in samples_AT_noise.iterkeys():
        out_data[sample] = [ d[sample] for d in ds ] 

    print(out_data.items()[0:3])
    outfile.write(",".join(names) + "\n")
    for sample, values in out_data.items():
        outfile.write("%s,%s\n" % (sample, ','.join(map(str, values))))
        
    return


def plot_vs_prod(wkdir, flowcell_name):
   
    # put a renamed copy of the special case sample plots for the lowest passing sample in the plots directory
    infile = open(wkdir + "metrics/all_metrics.table", "r")
    lowest_passing_sample = get_passing_samples(infile, lowest=True)
    os.system("cp %s/plots/AT_plot*%s*.png %s/plots/AT_plot_LOWEST.png" % (wkdir, lowest_passing_sample, wkdir))
    os.system("cp %s/plots/CAH_plot*%s*.png %s/plots/CAH_plot_LOWEST.png" % (wkdir, lowest_passing_sample, wkdir))
    os.system("cp %s/plots/GBA_cnvs*%s*.png %s/plots/GBA_plot_LOWEST.png" % (wkdir, lowest_passing_sample, wkdir))
    os.system("cp %s/plots/SMA_scatter*%s*.png %s/plots/SMA_plot_LOWEST.png" % (wkdir, lowest_passing_sample, wkdir))
    
    # create histograms for the batch
    if os.path.exists(wkdir + "metrics/df_special_case_noise.csv"):
        
        twelve_week_data = pd.read_csv('/rnd/bgould/SDTS/All_metrics_compare/12_wk_special_noise_metrics.csv', index_col=0)
        twelve_week_data = twelve_week_data.drop('sample', axis=0) # remove header rows
        
        flowcell_data = pd.read_csv(wkdir + "metrics/df_special_case_noise.csv")
        flowcell_data =flowcell_data.set_index('sample')

        # a generic histogram plotter:
        for stat in twelve_week_data.columns:
            plt.figure(randint(0,1000000))
            sns.distplot(twelve_week_data[stat].astype(float), hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label="Production Data")
            sns.distplot(flowcell_data[stat], hist=False, kde=True, kde_kws={'shade':True, 'linewidth':3}, label=str(flowcell_name))
            plt.legend(prop={'size':16})
            plt.title(str(stat))
            plt.xlabel(str(stat))
            plt.ylabel('density')

            plt.savefig(wkdir + "/plots/" + str(stat) + "_compare.png")
            print( str(stat) + " plotted.")
    else:
        print("Calculate noise metrics before plotting.")    


if __name__ == "__main__":
    fire.Fire()
        
