#! /bin/bash

DS=0.5

declare -a SAMPLES=( "5517CULTURE1" "5518CULTURE1" "5520BUFFY" "5520PELLET" "5520SUPE" "5520PLASMA")

declare -a COV=( 34.0 37.7 28.1 35.5 38.5 21.7)

index=$(( $SGE_TASK_ID - 1 ))
S=${SAMPLES[${index}]}

C=${COV[${index}]}

F=$(echo "scale=3 ; ${DS}/${C}" |bc)

echo $S
echo $DS
echo $F

mkdir -p ${DS}X

samtools view -b -s $F -@ 20 laneM_${S}_sort.bam > downsampled_${DS}X/${S}_${DS}X.bam 

echo "Completed downsample"
