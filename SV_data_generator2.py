## This script will generate simulated SVs from input bed files using coordinate sorted bam files. Output will preserve paired reads.


import sys
import pysam

sys.stderr.write("Remember: Input reads must be Name Sorted!")

if len(sys.argv) < 5:
    sys.stderr.write("\n\nUseage: python SV_data_generator.py SVs.bed SVs_comp.bed infile.bam downsample_to_cov samplename > outfile.bam \n")
    sys.exit()

sample_coverages = { "laneM_5517CULTURE1":34.0 , "laneM_5518CULTURE1":37.7 , "laneM_5520PELLET":35.5 , "laneM_5520BUFFY":28.1 }

sample = str(sys.argv[5])

downsample_prob = float(sys.argv[4])/(sample_coverages[sample])

bamfile = pysam.AlignmentFile(sys.argv[3], "rb")

SV_samp_prob = float(downsample_prob)*0.5

SVs = open(sys.argv[1], "r")
for line in SVs:
    chrom = line.split()[0]  # define the next deletion interval
    start = line.split()[1]
    end = line.split()[2]
    pysam.view("-s", str(SV_samp_prob), "-o", (str(sys.argv[2]) + ".TMP1"), str(chrom), ":", str(start), "-", str(end))   #this call should preserve read pairing

nonSVs = open(sys.argv[2], "r")
for line in nonSVs:
    chrom = line.split()[0]
    start = int(line.split()[1])
    end = int(line.split()[2])
    pysam.view("-s", str(SV_samp_prob), "-o", (str(sys.argv[2]) + ".TMP2"), str(chrom), ":", str(start), "-", str(end))

bamfile.close()
nonSVs.close()
SVs.close()

sys.stderr.write("Completed subsampling deletions. Coverage: " + str(sys.argv[4]) )